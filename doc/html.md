[HTML5 Boilerplate homepage](https://html5boilerplate.com/) | [Documentation
table of contents](TOC.md)

# The HTML

By default, HTML5 Boilerplate provides two `html` pages:

* [`index.html`](#indexhtml) - a default HTML skeleton that should form the
  basis of all pages on your website
* `404.html` - a placeholder 404 error page

## `index.html`


### The order of the `<title>` and `<meta>` tags

The charset declaration (`<meta charset="utf-8">`) must be included completely
within the [first 1024 bytes of the
document](https://html.spec.whatwg.org/multipage/semantics.html#charset)
and should be specified as early as possible (before any content that could be
controlled by an attacker, such as a `<title>` element) in order to avoid a
potential [encoding-related security
issue](https://code.google.com/archive/p/doctype-mirror/wikis/ArticleUtf7.wiki)
in Internet Explorer


Google's [Create good meta
descriptions](https://support.google.com/webmasters/answer/35624?hl=en#meta-descriptions)
documentation has useful tips on creating an effective description.

### Mobile Viewport

There are a few different options that you can use with the [`viewport` meta
tag](https://docs.google.com/present/view?id=dkx3qtm_22dxsrgcf4 "Viewport and
Media Queries - The Complete Idiot's Guide"). You can find out more in [the MDN
Web
Docs](https://developer.mozilla.org/en-US/docs/Mozilla/Mobile/Viewport_meta_tag).
HTML5 Boilerplate comes with a simple setup that strikes a good balance for
general use cases.

```html
<meta name="viewport" content="width=device-width, initial-scale=1">
```

If you want to take advantage of edge-to-edge displays of iPhone X/XS/XR you
can do so with additional viewport parameters. [Check the WebKit
blog](https://webkit.org/blog/7929/designing-websites-for-iphone-x/) for
details.


### Web App Manifest

HTML5 Boilerplate includes a simple web app manifest file.

The web app manifest is a simple JSON file that allows you to control how your
app appears on a device's home screen, what it looks like when it launches in
that context and what happens when it is launched. This allows for much greater`
control over the UI of a saved site or web app on a mobile device.

It's linked to from the HTML as follows:

```html
<link rel="manifest" href="site.webmanifest">
```

Our
[site.webmanifest](https://github.com/h5bp/html5-boilerplate/blob/master/src/site.webmanifest)
contains a very skeletal "app" definition, just to show the basic usage. You
should fill this file out with [more information about your site or
application](https://developer.mozilla.org/en-US/docs/Web/Manifest)

### Favicons and Touch Icon

The shortcut icons should be put in the root directory of your site.
`favicon.ico` is automatically picked up by browsers if it's placed in the root.
HTML5 Boilerplate comes with a default set of icons (include favicon and one
Apple Touch Icon) that you can use as a baseline to create your own.

Please refer to the more detailed description in the [Extend section](extend.md)
of these docs.


### What About Polyfills?

If you need to include
[polyfills](https://remysharp.com/2010/10/08/what-is-a-polyfill) in your
project, you must make sure those load before any other JavaScript. If you're
using a polyfill CDN service, like [polyfill.io](https://polyfill.io/v3/), just put
it before the other scripts in the bottom of the page:

```html
    <script src="js/vendor/modernizr-3.10.0.min.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>
```

If you like to just include the polyfills yourself, you could include them in
`js/plugins.js`. When you have a bunch of polyfills to load in, you could also
create a `polyfills.js` file in the `js/vendor` directory or include the files
individually and combine them using a build tool. Always ensure that the
polyfills are all loaded before any other JavaScript.




