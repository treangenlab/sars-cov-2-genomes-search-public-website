namespace Model.CdcVariantOf
{
    const validCdcLabelNames =
        ["Alpha",  "Beta",  "Gamma", "Epsilon", "Eta", "Iota", "Kappa", "Mu", "Zeta", "Delta", "Omicron"]

    export class CdcLabel implements CdcLabel
    {
        /**
         * An uppercase representation of this pango.
         */
        readonly asString: string

        constructor(cdcLabel: string)
        {
            this.asString = cdcLabel
            if (validCdcLabelNames.indexOf(this.asString) == -1)
                throw Error("Malformed cdc label: " + cdcLabel)
        }

        // n.b. needed because tabulator renders cell values based on `toString()`
        toString()
        {
            return this.asString
        }
    }
}
