namespace Model.UrlFetching
{
    /**
     * An object that can fetch a string represented url via the browser's Fetch API.
     */
    export interface UrlFetcher
    {
        /**
         * Starts the process of fetching a resource from the network, returning a promise which is fulfilled once the
         * response is available.
         *
         * @param url the URL of the resource to fetch.
         * @param init an object containing any custom settings that you want to apply to the request.
         */
        fetchAsync(url: string, init?: RequestInit): Promise<Response>;
    }

    /**
     * A fetcher that allows at most a constructor provided number of http transactions to occur at the same time. Any
     * new `fetchAsync` operations that are invoked when the maximum number of parallel requests are already in flight
     * will be queued for future processing once existing network transactions are completed.
     */
    export class UrlFetcherParallelizationLimited implements UrlFetcher
    {
        /**
         * The number of http transactions fetched but not yet completed.
         */
        private numRequestsInFlight = 0

        /**
         * Function wrappers wrapping request operations that have not been started yet, but should be started in the
         * future.
         */
        private requestQueue: (() => void)[] = []

        /**
         * @param parallelizationLimit the maximum number of http transactions this fetcher execute in parallel at any
         *        given time. must be >= 1.
         * @throws an error if `parallelizationLimit` is <= 0.
         */
        constructor(readonly parallelizationLimit: number)
        {
            if(parallelizationLimit < 1)
                throw Error("concurrencyLimit must be >= 1. found: " + parallelizationLimit)
        }

        // contract from super
        fetchAsync(url: string, init?: RequestInit): Promise<Response>
        {
            return new Promise<Response>((resolve, reject) =>
            {
                const performFetchOfUrl: (() => void) = () =>
                {
                    fetch(url, init).then((response: Response) => { this.handleFetchCompleted(); resolve(response) })
                                    .catch((reason: any)       => { this.handleFetchCompleted(); reject(reason) })
                }

                if(this.numRequestsInFlight < this.parallelizationLimit)
                {
                    performFetchOfUrl()
                    this.numRequestsInFlight++
                }
                else
                {
                    this.requestQueue.push(performFetchOfUrl)
                }
            })
        }

        private handleFetchCompleted()
        {
            this.numRequestsInFlight--

            /*
             * If the request queue is non-empty, dequeue and dispatch the next request.
             */
            const performFetchOfUrl: ((() => void)) | undefined = this.requestQueue.shift()

            if(performFetchOfUrl === undefined) // if queue is empty
                return

            performFetchOfUrl()
            this.numRequestsInFlight++

        }
    }
}