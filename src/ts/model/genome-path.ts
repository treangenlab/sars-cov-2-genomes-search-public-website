namespace Model.Genomes
{
    // given an NCBI accession id, create the corresponding path for the json file in /data
    // e.g. ERR416504 -> /data/genomes/ncbi/sra/ERR/416/504/ERR416504.json
    export function makeNcbiGenomeDataPath(accessionId: string): string
    {
        let accessionIdUpper = accessionId.toUpperCase() // case is important to some web servers and both directory
        let pathAccum = "/data/genomes/ncbi/sra/"        // names and file name will be uppercase on server
        let segmentAccum = ""
        for (let c of accessionIdUpper)
        {
            segmentAccum += c
            if (segmentAccum.length === 3)
            {
                pathAccum += segmentAccum + "/"
                segmentAccum = ""
            }
        }

        pathAccum += accessionIdUpper + ".json"
        return pathAccum
    }
}