namespace Model.DataOrigin
{
    /**
     * Models the online archives from which genomic data originates.
     */
    export type Repository = "NCBI" // will add | "xyz" terms as the repos we source from increase

    export function parseRepository(text: string): Repository | null
    {
        if (text.toUpperCase() === "NCBI")
            return "NCBI"

        return null
    }
}
