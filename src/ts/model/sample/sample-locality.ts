namespace Model.Sample
{
    import Phrase = DataStructures.Strings.Phrase

    /**
     * The place at which a genetic sample was taken.
     */
    // n.b. class instead of interface because instances will be cloned by the structured clone algorithm the from web
    // worker to the UI thread and as such will be which striped of any non-properties.
    export class SampleLocality
    {
        /**
         * The name of the region or null if unknown.
         */
        readonly region: Phrase | null

        /**
         * The name of the locality or null if unknown.
         */
        readonly locality: Phrase | null

        /**
         * The name of the country.
         */
        readonly country: Phrase

        constructor(region: Phrase | null, name: Phrase | null, country: Phrase)
        {
            this.region = region
            this.locality = name
            this.country = country
        }

        /**
         * @param clone an object that is a structured clone of an `SampleLocation` instance.
         * @return a corresponding GenomicIndexEntry` instance or `null` if the clone is not a 'SampleLocation'
         *         structured clone
         */
        public static makeFromStructuredClone(clone: SampleLocalityStructuredClone): SampleLocality
        {
            const region   = clone.region   === null ? null : new Phrase(clone.region.asString)
            const locality = clone.locality === null ? null : new Phrase(clone.locality.asString)
            const country = new Phrase(clone.country.asString)
            return new SampleLocality(region, locality, country)
        }
    }

    /**
     * The property structure of an a `SampleLocality` instance that has been structurally cloned.
     */
    export type SampleLocalityStructuredClone =
    {
        region:   { asString: string } | null,
        locality: { asString: string } | null,
        country:  { asString: string }
    }
}