namespace Model.Sample
{
    import Repository = Model.DataOrigin.Repository
    import CdcLabel = Model.CdcVariantOf.CdcLabel
    import parseRepository = Model.DataOrigin.parseRepository
    import Pango = Model.Pango.Pango
    import Phrase = DataStructures.Strings.Phrase

    /**
     * A record of some Covid sample.
     */
    // n.b. class instead of interface because instances will be cloned by the structured clone algorithm the from web
    // worker to the UI thread and as such will be which striped of any non-properties.
    export class SampleRecord
    {
        /**
         * The data repository from which the sample's original digital representation (e.g. reads, assembled genome)
         * resides.
         */
        readonly repository: Repository

        /**
         * The repository local unique id of the genome. This id is only guaranteed to be unique within respect to ids
         * from the same repository (e.g. NCBI SRA database, ENA database, etc.).
         */
        readonly accessionId: string

        /**
         * The Pango lineage label of the genome or null if not assigned to a lineage.
         */
        readonly pango: Pango | null

        /**
         * The CDC label of the genome or null if not assigned a label.
         */
        readonly cdcLabel: CdcLabel | null

        /**
         * The geographic location where the sample was discovered.
         */
        readonly sampleLocality: SampleLocality

        /**
         * The calendar date on which the sample was collected in an unspecified timezone. The values of any time
         * related fields of the date are unspecified.
         */
        readonly whenCollected: Date

        constructor(repository: Repository,  accessionId: string, pango: Pango | null, cdcLabel: CdcLabel | null,
                    sampleLocality: SampleLocality,  whenCollected: Date)
        {
            this.repository = repository
            this.accessionId = accessionId
            this.pango = pango
            this.cdcLabel = cdcLabel
            this.sampleLocality = sampleLocality
            this.whenCollected = whenCollected
        }

        /**
         * @param clone an object that is a structured clone of an `GenomicIndexEntry` instance.
         * @return a corresponding GenomicIndexEntry` instance or `null` if the clone is not a 'GenomicIndexEntry'
         *         structured clone
         */
        static makeFromStructuredClone(clone: SampleRecordStructuredClone): SampleRecord | null
        {
            const locality: SampleLocality = SampleLocality.makeFromStructuredClone(clone.sampleLocality)
            const repository: Repository | null = parseRepository(clone.repository)
            if(repository === null)
                return null
            const pango = clone.pango == null ? null : new Pango(clone.pango.asString)
            const cdcLabel = clone.cdcLabel == null ? null : new CdcLabel(clone.cdcLabel.asString)

            return new SampleRecord(repository, clone.accessionId, pango, cdcLabel, locality, clone.whenCollected)
        }

        static parseFromJson(text: string): SampleRecord
        {
            // TODO: add support for ENA and GISAID

            const entryRaw = JSON.parse(text)

            if (entryRaw.o === undefined)
                throw Error("Given json object does not have a 'o' field. Given: " + text)

            const dataOrigin = entryRaw.o

            let pango: Pango | null = null;
            if (entryRaw.p !== undefined)
            {
                if (typeof entryRaw.p !== 'string')
                    throw Error("Given json object does not have a 'p' field of type string. Given: " + text)

                pango = new Pango(entryRaw.p);
            }

            let cdcLabel: CdcLabel | null = null;
            if (entryRaw.c !== undefined)
            {
                if (typeof entryRaw.c !== 'string')
                    throw Error("Given json object does not have a 'c' field of type string. Given: " + text)

                cdcLabel = new CdcLabel(entryRaw.c);
            }

            if (dataOrigin.r === undefined)
                throw Error("Given json object does not have a 'd.r' field. Given: " + text)

            if (typeof dataOrigin.r !== 'string')
                throw Error("Given json object does not have a 'o.r' field of type string. Given: " + text)

            // @ts-ignore
            const repository: Repository | null = parseRepository(dataOrigin.r)

            if (repository === null)
                throw Error("Unknown repository " + dataOrigin.r)

            if (entryRaw.l === undefined)
                throw Error("Given json object does not have a 'l' field. Given: " + text)

            if (entryRaw.l.c === undefined)
                throw Error("Given json object does not have a 'l.c' field. Given: " + text)

            const region:   Phrase | null = entryRaw.l.r === undefined ? null : new Phrase(entryRaw.l.r.toString())
            const locality: Phrase | null = entryRaw.l.l === undefined ? null : new Phrase(entryRaw.l.l.toString())
            const country: Phrase = new Phrase(entryRaw.l.c.toString())

            const sampleLocality = new SampleLocality(region, locality, country)

            if (entryRaw.w === undefined)
                throw Error("Given json object does not have a 'w' field. Given: " + text)

            const year = parseInt(entryRaw.w.y)
            const month = parseInt(entryRaw.w.m)
            const date = parseInt(entryRaw.w.d)

            switch (repository)
            {
                case "NCBI":

                    if (dataOrigin.i === undefined)
                        throw Error("Given json object does not have a 'o.i' field. Given: " + text)

                    if (typeof dataOrigin.i !== 'string')
                        throw Error("Given json object's 'o.i' field is not of type string. Given: " + text)

                    return new SampleRecord(repository, dataOrigin.i, pango, cdcLabel, sampleLocality,
                        new Date(year, month-1, date))
                default:
                    return function unreachable(): never
                    {
                        throw new Error("unreachable")
                    }()
            }
        }
    }

    /**
     * The property structure of an a `SampleRecord` instance that has been structurally cloned.
     */
    export type SampleRecordStructuredClone =
    {
        repository: string,
        accessionId: string,
        pango: { asString: string } | null,
        cdcLabel: { asString: string } | null,
        whenCollected: Date
        sampleLocality: SampleLocalityStructuredClone
    }
}