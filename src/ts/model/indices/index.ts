namespace Model.Indices
{
    import LogicalSetReadOnly = DataStructures.Sets.LogicalSetReadOnly
    import LowerCaseString = DataStructures.Strings.LowerCaseString
    import ObjectIteratorAsync = DataStructures.Iterator.ObjectIteratorAsync
    import SampleRecord = Model.Sample.SampleRecord
    import ObjectIteratorAsyncBase = DataStructures.Iterator.ObjectIteratorAsyncBase
    import SetLowerCaseString = DataStructures.Sets.SetLowerCaseString
    import LogicalSet = DataStructures.Sets.LogicalSet
    import makeNcbiGenomeDataPath = Model.Genomes.makeNcbiGenomeDataPath
    import UrlFetcher = Model.UrlFetching.UrlFetcher
    import UrlFetcherParallelizationLimited = Model.UrlFetching.UrlFetcherParallelizationLimited

    /**
     * Disjoint sets of samples grouped by an implementation specific sample feature. E.g. a Pango linage index where
     * each set of samples within the index have a common Pango lineage.
     */
    export interface Index
    {
        /**
         * Fetches from the index the set of distinct feature values (i.e. group names) among all samples.
         *
         * @return a set of distinct feature values.
         */
        fetchDistinctFeatureValuesAsync(): Promise<LogicalSetReadOnly<LowerCaseString>>

        /**
         * Fetches from the index the set of samples that has the given value for the indexed feature. E.g. the set of
         * samples with a Pango value of b.1.1.28.
         *
         * If no set of samples exists within the index that have the given feature value, returns a zero length
         * iterator.
         *
         * @param value the feature value for which to search.
         * @return the set of samples within the index with members that all share the given value for the indexed
         *         feature.
         */
        fetchSamplesWithFeatureValue(value: LowerCaseString | number): ObjectIteratorAsync<SampleRecord>
    }

    /**
     * An index implementation where the sets are stored on a remote server.
     */
    export class ServerSideIndex implements Index
    {
        /**
         * @param baseUrl the url prefix for all server resources related to the index.
         * @param fetcher an optional fetcher to be used when making http requests to the server.
         */
        constructor(private readonly baseUrl: string,
                    protected readonly fetcher: UrlFetcher = new UrlFetcherParallelizationLimited(100))
        {
        }

        // contract from super
        fetchDistinctFeatureValuesAsync(): Promise<LogicalSetReadOnly<LowerCaseString>>
        {
            const indexManifestUrl = this.baseUrl + "_.txt"

            return new Promise<LogicalSetReadOnly<LowerCaseString>>((resolve, reject) =>
            {
                this.fetcher.fetchAsync(indexManifestUrl).then((response: Response) =>
                {
                    if(!response.ok)
                    {
                        reject("bad response code " + response.status)
                        return
                    }

                    return response.text().then((body: string) =>
                    {
                        const entriesAccum: LogicalSet<LowerCaseString> = new SetLowerCaseString();
                        for(let line of body.split('\n'))
                            entriesAccum.add(new LowerCaseString(line))

                        resolve(entriesAccum)
                    })
                })
                .catch(e => reject(e))
            })

        }

        // contract from super
        fetchSamplesWithFeatureValue(value: LowerCaseString | number): ObjectIteratorAsync<SampleRecord>
        {
            const url = this.baseUrl + makeIndexFileName(value)

            /*
             * Acquire a reader to the HTTP response body of a GET request to the url.
             */
            const bodyReader: Promise<SimpleReadableStreamReader> = fetchIndexBodyReader(url, this.fetcher)

            // if the url resource does not exist (i.e. server returns status 404) a zero length iterator is returned
            return new SampleRecordIterator(bodyReader)
        }
    }

    /**
     * The server side id index.
     */
    export class ServerSideIndexLocalId extends ServerSideIndex
    {
        fetchSamplesWithFeatureValue(value: LowerCaseString): ObjectIteratorAsync<SampleRecord>
        {
            const url = makeNcbiGenomeDataPath(value.toString())

            /*
             * Acquire a reader to the HTTP response body of a GET request to the url.
             */
            const bodyReader: Promise<SimpleReadableStreamReader> = fetchIndexBodyReader(url, this.fetcher)

            // if the url resource does not exist (i.e. server returns status 404) a zero length iterator is returned
            return new SampleRecordIterator(bodyReader)
        }
    }

    // return a reader of the HTTP body of a genome index at the given url. deal with status code 404 by returning an
    // empty reader.
    function fetchIndexBodyReader(url: string, fetcher: UrlFetcher): Promise<SimpleReadableStreamReader>
    {
        return new Promise<SimpleReadableStreamReader>((resolve, reject) =>
        {
            fetcher.fetchAsync(url)
            .then((response: Response) =>
            {
                const contentType: string | null = response.headers.get('content-type')
                const isContentTypeActuallyOrProbablyUtf8 = contentType === `text/plain; charset=utf-8` ||
                                                            contentType === `text/plain;charset=utf-8`  ||
                                                            contentType === `application/json;charset=utf-8`  ||
                                                            // assume utf-8 if not stated
                                                            contentType === `application/json`  ||
                                                            contentType === `text/plain`

                if (!response.ok)
                {
                    if (response.status === 404) // if there is no index at the address, return an empty reader
                    {
                        resolve(new class implements SimpleReadableStreamReader
                        {
                            read(): Promise<ReadableStreamReadResult<Uint8Array>>
                            {
                                return Promise.resolve({done: true, value: new Uint8Array(0)})
                            }
                        })
                    }
                    else
                    {
                        reject("Non-success HTTP status code " + response.status)
                    }
                }
                else if (contentType === null)
                {
                    reject("Missing content-type header")
                }
                else if (!isContentTypeActuallyOrProbablyUtf8)
                {
                    reject("Unexpected content-type " + contentType)
                }
                else if (response.body === null)
                {
                    reject("Response body was null")
                }
                else
                {
                    resolve(response.body.getReader())
                }
            })
            .catch((error) => reject(error))
        })
    }

    // on the server, index files don't use spaces. e.g. locality "new york city" has the filename "new_work_city.txt"
    function makeIndexFileName(featureValue: LowerCaseString | number): String
    {
        return featureValue.toString().replace(/ /g, "_") + '.txt'
    }

    /**
     * A subset of the methods available on ReadableStreamReader<Uint8Array> actually used by `SampleRecordIterator`.
     * Simplifies instantiation of an anonymous class in `fetchIndexBodyReader(...)` by not having to implement methods
     * not invoked.
     */
    type SimpleReadableStreamReader = { read(): Promise<ReadableStreamReadResult<Uint8Array>> }

    // Lazily accumulate characters from constructor bodyReader to _bodyBuffer as needed to discover lines of the HTTP
    // response body. When enough characters to form a line (signaled by \n or end of the body) are buffered,
    // parse the line to a SampleRecord instance and make the instance available via getNextAsync()'s returned
    // promise.
    class SampleRecordIterator extends ObjectIteratorAsyncBase<SampleRecord>
    {
        /**
         * A buffer that holds sequential character reads from bodyReader that have not yet been parsed and returned
         * by the iterator. Once a full line of characters is buffered, parsed, and returned, the parsed characters
         * are removed from the front of the buffer leaving any still yet unparsed characters at the end.  The
         * buffer may at some time hold 0, 1, or more than 1 full lines of the HTTP response.
         *
         * The first character of non-empty buffer will always be the start of a line of the HTTP response body.
         */
        private _bodyBuffer = ""

        private _utf8Decoder = new TextDecoder('utf-8')

        // null indicates that the reader (initially non-null via constructor) has been exhaustively read
        private bodyReader: Promise<SimpleReadableStreamReader> | null

        constructor(bodyReader: Promise<SimpleReadableStreamReader>)
        {
            super()
            this.bodyReader = bodyReader
        }


        // contract from interface
        getNextAsync(): Promise<SampleRecord | null>
        {
            return new Promise<SampleRecord | null>((resolve, reject) =>
            {
                const resolveOrRejectOrRecur = () =>
                {
                    /*
                     * If we have already buffered enough characters to have a full line, parse the line and return.
                     *
                     * Otherwise, keep reading characters until we have a line of characters (or end of response is
                     * reached).
                     */
                    const firstNewlinePos = this._bodyBuffer.indexOf("\n")

                    if (firstNewlinePos >= 0) // have we buffered at least 1 line? if so, parse and return it
                    {
                        const line = this._bodyBuffer.substr(0, firstNewlinePos)
                        resolve(SampleRecord.parseFromJson(line))
                        this._bodyBuffer = this._bodyBuffer.substr(firstNewlinePos + 1)
                        return
                    }

                    // we have not buffered at least one line. keep reading more characters until at least
                    // one line is buffered and then parse/return that line. or if response is exhausted, yield null
                    this.tryAppendMoreResponseBodyToBodyBuffer().then((numCharsAppended: number) =>
                    {
                        if (numCharsAppended === 0) // if reader has no more chars to give us
                        {
                            if (this._bodyBuffer.length > 0) // any last un-returned line terminated by end of body?
                            {
                                resolve(SampleRecord.parseFromJson(this._bodyBuffer))
                                this._bodyBuffer = ""
                            }
                            else // all entries already returned
                            {
                                resolve(null)
                            }
                        }
                        else // we have buffered more more characters to _bodyBuffer, try again to fulfil getNext()
                        {
                            resolveOrRejectOrRecur()
                        }
                    }).catch(error => reject(error))
                }
                resolveOrRejectOrRecur() // kick off (potential) recursion
            })
        }

        /**
         * Try to asynchronously read more characters from bodyReader and append them to the end of _bodyBuffer.
         * Returns the number of characters appended. Only in the case where the reader has been fully read will the
         * method return 0.
         */
        private tryAppendMoreResponseBodyToBodyBuffer(): Promise<number>
        {
            return new Promise<number>((resolve, reject) =>
            {
                if (this.bodyReader === null) // if we have previously exhausted the reader by invocations of this method
                {
                    resolve(0) // reader is exhausted, we have no more body characters to append to _bodyBuffer
                    return
                }

                this.bodyReader.then((reader: SimpleReadableStreamReader) =>
                {
                    reader.read()
                    .then((bodyPart: ReadableStreamReadResult<Uint8Array>) =>
                    {
                        if (bodyPart.done) // if no more characters in reader
                        {
                            resolve(0) // signal reader is fully consumed
                            this.bodyReader = null // set state reader is fully read. see bodyReader decl comment
                        }
                        else if (bodyPart.value.length >= 1) // if reader delivered next new body chars
                        {
                            const bodyPartAsString = this._utf8Decoder.decode(bodyPart.value)
                            this._bodyBuffer += bodyPartAsString
                            resolve(bodyPartAsString.length)
                        }
                        else
                        {
                            reject("reader is not done, but got empty read chunk")
                        }
                    })
                    .catch((error) => reject(error))
                })
            })
        }
    }
}