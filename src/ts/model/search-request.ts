namespace Model.SearchRequest
{
    import Phrase = DataStructures.Strings.Phrase

    /**
     * Parent type for the subclasses `SearchRequestByDateRange` and `SearchRequestByPhraseSet`, each representing
     * the possible types of searching that can be requested.
     */
    export abstract class SearchRequest
    {
        // visitor pattern
        abstract which<R>(forPhraseSetCase:(_:SearchRequestByPhraseSet) => R,
                          forDateRangeCase:(_:SearchRequestByDateRange) => R): R


        /**
         * Creates a `SearchRequestByDateRange` or `SearchRequestByPhraseSet` from the structured clone of a
         * `SearchRequestByDateRange` or `SearchRequestByPhraseSet`.
         *
         * @param clone a structured clone of a `SearchRequestByDateRange` or `SearchRequestByPhraseSet` value.
         * @return a new `SearchRequestByDateRange` or `SearchRequestByPhraseSet` instance corresponding to the clone or
         *         `null` if the clone was not a `SearchRequestByDateRange` structured clone
         */
        static makeFromStructuredClone(clone: { mode: number, phrases: { elements: readonly { asString: string }[] } } |
                                              { minDate: Date, maxDate: Date })
            : SearchRequest | null
        {
            if("mode" in clone)
                return SearchRequestByPhraseSet.makeFromStructuredClone(clone)
            else
                return SearchRequestByDateRange.makeFromStructuredClone(clone)
        }
    }

    /**
     * A search for records on or between the two moments.
     */
    export class SearchRequestByDateRange extends SearchRequest
    {
        /**
         * A moment before or equal to `maxDate`.
         */
        readonly minDate: Date

        /**
         * A moment after or equal to `minDate`.
         */
        readonly maxDate: Date

        constructor(date1: Date, date2: Date)
        {
            super();
            this.minDate = new Date(Math.min(date1.getTime(), date2.getTime()))
            this.maxDate = new Date(Math.max(date1.getTime(), date2.getTime()))
        }

        /**
         * Creates a `SearchRequestByDateRange` from the structured clone of a `SearchRequestByDateRange`.
         *
         * @param clone a structured clone of a `SearchRequestByDateRange` value.
         * @return a new `SearchRequestByDateRange` instance corresponding to the clone or `null` if the clone was not a
         *         `SearchRequestByDateRange` structured clone
         */
        // class doesnt currently have instance methods so this method isn't strictly needed, but good to establish
        // workflow now in case methods added in future.
        static makeFromStructuredClone(clone: { minDate: Date, maxDate: Date }): SearchRequestByDateRange | null
        {
            if(clone.maxDate.getTime() < clone.minDate.getTime())
                return null

            return new SearchRequestByDateRange(clone.minDate, clone.maxDate)
        }

        which<R>(_: (_: Model.SearchRequest.SearchRequestByPhraseSet) => R,
              forDateRangeCase: (_: Model.SearchRequest.SearchRequestByDateRange) => R): R
        {
            return forDateRangeCase(this)
        }
    }

    /**
     * Models a user submitted directive to search for records containing the given strings.
     */
    // n.b. class over interface because intended to be copied by the structured clone algorithm which strips out any
    // non-properties from the clone.
    export class SearchRequestByPhraseSet extends SearchRequest
    {

        /**
         * If there are multiple phrases, whether the phrases should be tread as ANDed or ORed together while searching.
         */
        readonly mode: SearchMode

        /**
         * The strings to look for when searching.
         */
        readonly phrases: PhraseSet

        // fill constructor
        constructor(mode: SearchMode, phrases: PhraseSet)
        {
            super()
            this.mode = mode
            this.phrases = phrases
        }

        /**
         * Creates a `SearchRequestByPhraseSet` from the structured clone of a `SearchRequestByPhraseSet`.
         *
         * @param clone a structured clone of a `SearchRequestByPhraseSet` value.
         * @return a new `SearchRequestByPhraseSet` instance corresponding to the clone or `null` if the clone was not a
         *         `SearchRequestByPhraseSet` structured clone
         */
        // class doesnt currently have instance methods so this method isn't strictly needed, but good to establish
        // workflow now in case methods added in future.
        static makeFromStructuredClone(clone: { mode: number, phrases: { elements: readonly { asString: string }[] } })
            : SearchRequest | null
        {
            let mode: SearchMode | null = null
            if(clone.mode === SearchMode.OR)
                mode = SearchMode.OR

            if(clone.mode === SearchMode.AND)
                mode = SearchMode.AND

            const phrases: PhraseSet | null = PhraseSet.makeFromStructuredClone(clone.phrases)

            if(phrases === null || mode === null)
                return null

            return new SearchRequestByPhraseSet(mode, phrases)
        }

        which<R>(forPhraseSetCase: (_: SearchRequestByPhraseSet) => R, _: (_: SearchRequestByDateRange) => R): R
        {
           return forPhraseSetCase(this)
        }
    }

    /**
     * A non-empty set of distinct phrase strings.
     */
    // n.b. class over interface because intended to be copied by the structured clone algorithm which strips out any
    // non-properties from the clone.
    export class PhraseSet implements Iterable<Phrase>
    {
        /**
         * The elements of the set.
         */
        readonly elements: readonly Phrase[]

        /**
         * Creates the non-empty set from the given source. Any duplicate phrase strings will be represented in the set
         * only once.
         *
         * If the given source is empty, an error is thrown.
         * If the given source is a sequence of strings and any string is not a valid `Phrase` value, an error is
         * thrown.
         *
         * @param elements the elements of the set
         */
        constructor(elements: Iterable<Phrase> | Iterable<string>)
        {
            const input: Phrase[] = []
            for(let e of elements)
            {
                const element: Phrase | string = e
                if(typeof element === 'string')
                    input.push(new Phrase(element))
                else
                    input.push(element)
            }

            const elementsAccum: Phrase[] = []
            outerLoop: for(let element of input)
            {
                for(let e of elementsAccum)
                    if(e.asString === element.asString)
                        continue outerLoop

                elementsAccum.push(element)
            }

            if(elementsAccum.length === 0)
                throw Error("elements may not be empty")

            this.elements = elementsAccum
        }

        /**
         * Creates a `PhraseSet` from the structured clone of a `PhraseSet`.
         *
         * @param clone a structured clone of a `PhraseSet` value.
         * @return a new `PhraseSet` instance corresponding to the clone or `null` if the clone was not a `PhraseSet`
         *         structured clone
         */
        static makeFromStructuredClone(clone: { elements: readonly { asString: string }[] } ): PhraseSet | null
        {
            const phrases: Phrase[] = []
            for(let element of clone.elements)
            {
                let phrase
                try
                {
                    phrase = new Phrase(element.asString)
                }
                catch (error)
                {
                    return null;
                }
                phrases.push(phrase)
            }

            return new PhraseSet(phrases)
        }

        // contract from interface
        [Symbol.iterator](): Iterator<Phrase>
        {
            return this.elements[Symbol.iterator]()
        }
    }

    /**
     * Indicates whether records must contain any or all search phrases.
     */
    export enum SearchMode
    {
        AND, OR
    }
}