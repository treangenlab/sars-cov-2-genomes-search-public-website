namespace Model.ViewModel
{
    /**
     * A string value that conforms to a given regular expression.
     *
     * @param T the string literal regular expression pattern to which the string value conforms
     * @param constraintRegexPattern the the string literal regular expression pattern to which the string value
     *                               conforms
     */
    export class RegularString<T extends string>
    {
        // n.b. it is important here that constraintRegexPattern be a field of the class (even though unused after
        // construction) so that the compiler will flag usages of the type with different string values for T
        // as incompatible.  Otherwise `RegularString<"A"> i = new RegularString<"B">(...)` will compile.
        constructor(private readonly value: string, private readonly constraintRegexPattern: T)
        {
            if (!new RegExp(constraintRegexPattern).test(value))
                throw new Error("Given value does not conform to regex " +
                    this.constraintRegexPattern + ". found: " + value)
        }

        toString()
        {
            return this.value
        }
    }

    /**
     * The empty string, the string of exactly one non-whitespace character, or a string that starts and ends with
     * a non-whitespace character.
     */
    export class TrimmedString extends RegularString<"(^(\\S)(.*)(\\S)$)|\\S|(^$)">
    {
        constructor(value: string)
        {
            super(value, "(^(\\S)(.*)(\\S)$)|\\S|(^$)")
        }
    }
}
