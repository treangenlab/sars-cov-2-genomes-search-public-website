namespace Model.ViewModel
{
    /**
     * Defines a set of record summaries to be shown to the user. Two `RecordSummary` instances are considered
     * indistinguishable (and thus only represented in the set once) if they have the same `repository` string values
     * and `accessionId` string values.
     */
    export interface SearchResultsViewModel
    {
        /**
         * Adds each given result not already in the set to the set in the given order. Any summary already contained in
         * the set (either because the duplicate summary preexisted in the set prior to the invocation of add or because
         * added during invocation of this method as `results` is scanned in order), as defined by the class contract,
         * will be ignored.
         *
         * If this method is ever invoked after an invocation of `freeze()` an error will be thrown.
         *
         * @param results the elements to consider for addition to the set.
         */
        add(results: Iterable<VariantSummary>): void

        /**
         * Declares that no more summaries can be added to the set. Invoking `add(...)` after invoking this method
         * will result in `add` throwing an error.
         */
        freeze(): void

        /**
         * Registers the given listener with the set.  Upon each invocation of `add(...)`, but prior to its return,
         * each listener instance registered by this method will be invoked once and passed the (possibly empty) subset
         * of summaries passed to `add` that were added to the set (i.e. not considered duplicates).
         *
         * @param listener the callback
         */
        addNewResultsAddedListener(listener: (_: readonly VariantSummary[]) => void): void

        /**
         * Registers the given listener with the set.  Upon each invocation of `freeze()`, but prior to its return,
         * each listener instance registered by this method will be invoked once.
         *
         * @param listener the callback
         */
        addFreezeListener(listener: () => void): void
    }

    /**
     * Optional base class for implementations of `SearchResultsViewModel`.
     */
    export abstract class SearchResultsViewModelBase implements SearchResultsViewModel
    {
        // TODO: change to typed event?
        private readonly _newResultsAddedListeners: Set<((_: readonly VariantSummary[]) => void)> =
            new Set<((_: readonly VariantSummary[]) => void)>()

        private readonly _freezeListeners: Set<(() => void)> = new Set<() => void>()

        private _isFrozen: boolean = false

        protected get isFrozen(): boolean { return this._isFrozen }

        // contract from interface
        public addNewResultsAddedListener(listener: (_: readonly VariantSummary[]) => void): void
        {
            this._newResultsAddedListeners.add(listener)
        }

        // contract from interface
        public addFreezeListener(listener: () => void): void
        {
            this._freezeListeners.add(listener)
        }

        // contract from interface
        public freeze(): void
        {
            this._isFrozen = true
            for(let listener of this._freezeListeners)
                listener()
        }

        protected fireNewResultsAddedEvent(newResults: VariantSummary[])
        {
            for(let listener of this._newResultsAddedListeners)
                listener(newResults)
        }

        // contract from interface
        abstract add(results: Iterable<Model.ViewModel.VariantSummary>): void
    }

    /**
     * A SearchResultsViewModel implementation that uses a `Set<string>` to track which summaries are in the set.
     */
    export class SearchResultsViewModelStringSet extends SearchResultsViewModelBase
    {
        /**
         * The "global" ids of each summary in the set.
         */
        private readonly _ids: Set<string> = new Set<string>()

        // contract from interface
        add(results: Iterable<VariantSummary>): void
        {
            if(this.isFrozen)
                throw Error("Can't add results once frozen.")

            const newResultsAccum: VariantSummary[] = []
            for(let result of results)
            {
                const globalId = result.repository + ";" + result.accessionId
                if(this._ids.has(globalId))
                    continue

                this._ids.add(globalId)
                newResultsAccum.push(result)
            }

            this.fireNewResultsAddedEvent(newResultsAccum)
        }
    }
}