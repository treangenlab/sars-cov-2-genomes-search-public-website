namespace Model.ViewModel
{
    /**
     * When search results are displayed to the user, the UI displays list of records each element of which is modeled
     * by an instance of this interface.
     */
    export interface VariantSummary
    {
        /**
         * The data repository from which the records's original digital representation (e.g. reads, assembled genome)
         * resides.
         */
        readonly repository: "NCBI" // in the future | "ENA" | "GISAID"

        /**
         * The repository local unique id of the record's data origin. This id is only guaranteed to be unique within
         * respect to ids from the same repository (e.g. NCBI SRA database, ENA database, etc.).
         */
        readonly accessionId: RegularString<"^\\S+$">

        /**
         * The Pango lineage label of the record's sample.
         */
        readonly pango: TrimmedString

        /**
         * The CDC label of record's sample or the empty string if no label applies.
         *
         * See https://www.cdc.gov/coronavirus/2019-ncov/variants/variant-classifications.html
         */
        readonly cdcLabel: TrimmedString

        /**
         * The name of the locality in which the record's sample was collected or the empty string if unknown.
         */
        readonly sampleLocalityName: TrimmedString

        /**
         * The name of the region in which the record's sample was collected or the empty string if unknown.
         */
        readonly sampleLocalityRegion: TrimmedString

        /**
         * The name of the country in which the record's sample was collected. Never the empty string.
         */
        readonly sampleLocalityCountry: TrimmedString

        /**
         * The calendar date on which the sample was collected in an unspecified timezone. The values of any time
         * related fields of the date are unspecified.
         */
        readonly whenCollected: Date
    }
}