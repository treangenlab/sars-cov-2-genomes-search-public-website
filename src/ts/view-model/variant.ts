namespace Model.ViewModel
{
    /**
     * The data to show the user when viewing variant.html
     */
    export interface Variant
    {
        /**
         * The repository local unique id of the record's data origin. This id is only guaranteed to be unique within
         * respect to ids from the same repository (e.g. NCBI SRA database, ENA database, etc.).
         */
        id: RegularString<"^\\S+$">

        /**
         * The data repository from which the records's original digital representation (e.g. reads, assembled genome)
         * resides.
         */
        repository: "NCBI" // in the future | "ENA" | "GISAID"

        /**
         * The url to the VCF file of this variant.
         */
        vcfPath: RegularString<"^\\S+$">

        /**
         * The name of the locality in which the record's sample was collected or the empty string if unknown.
         */
        locality: TrimmedString

        /**
         * The name of the region in which the record's sample was collected or the empty string if unknown.
         */
        region: TrimmedString

        /**
         * The name of the country in which the record's sample was collected or the empty string if unknown.
         */
        country: TrimmedString

        /**
         * The Pango lineage label of the record's sample.
         */
        pango: TrimmedString

        /**
         * A list of comments associated with various positions within the variant.
         */
        positionNotes: PositionNote[]
    }

    export type PositionNote = { positionId: RegularString<"^\\S+$">, proteinId: RegularString<"^\\S+$">,
                                 mutations: RegularString<"^\\S+$">, comments: string[] };

}