namespace View
{
    /**
     * A view to display when the requested variant for display is unknown.
     */
    export class VariantNotFoundView implements ShowableView
    {
        show(): void
        {
            const notFoundDiv: HTMLElement | null = document.getElementById("not-found")
            if (notFoundDiv === null)
                throw Error("div element not found")

            notFoundDiv.style.display = 'flex'
        }
    }
}