namespace View
{
    /**
     * A view in the UI
     */
    export interface ShowableView // silly name because TypeScript won't allow interface View inside namespace View
    {
        /**
         * Ensure the view is visible to the user.
         */
        show(): void
    }
}