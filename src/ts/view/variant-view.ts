namespace View
{
    import Variant = Model.ViewModel.Variant
    import PositionNote = Model.ViewModel.PositionNote

    /**
     * A display of a specific variant's information.
     */
    export interface VariantView extends ShowableView
    {
        /**
         * An event that is raised when the user requests a downloadable copy of the mutations table.
         */
        userRequestedMutationsDownloadEvent: TypedEvent<void>

        // contract from super
        show(): void
    }

    /**
     * A `VariantView` implementation that is bound to the elements of variant.html
     */
    export class VariantViewHtml implements VariantView
    {
        /**
         * Controller for `userRequestedMutationsDownloadEvent`
         */
        private _userRequestedMutationsDownloadEventController = new TypedEventController<void>()

        // contract from super
        userRequestedMutationsDownloadEvent: TypedEvent<void> = this._userRequestedMutationsDownloadEventController

        /**
         * @param _vm the variant information to display
         */
        constructor(private readonly _vm: Variant)
        {
            document.getElementById("download-mutations")!.onclick = () =>
            {
                this._userRequestedMutationsDownloadEventController.fire()
            }
        }

        // contract from super
        show(): void
        {
            const detailsDiv = document.getElementById("details")
            if(detailsDiv === null)
                throw Error("details element not found")

            detailsDiv.style.display = 'block' // un-hide the div

            document.getElementById("id")!.innerText  = this._vm.id.toString()
            document.getElementById("vcf")!.innerText = this._vm.id.toString() + ".vcf"
            document.getElementById("vcf")!.setAttribute("href", this._vm.vcfPath.toString())

            switch (this._vm.repository)
            {
                case "NCBI":
                    const ncbiUrl = "https://www.ncbi.nlm.nih.gov/sra/?term=" + this._vm.id
                    document.getElementById("id")!.setAttribute("href", ncbiUrl)
                    break
                default:
                    const unreachable: never = this._vm.repository
                    throw Error("unreachable if program compiles " + unreachable)
            }

            document.getElementById("locality")!.innerText = this._vm.locality.toString()
            document.getElementById("region")!.innerText   = this._vm.region.toString()
            document.getElementById("country")!.innerText   = this._vm.country.toString()

            const pangoUrl = "https://cov-lineages.org/lineage.html?lineage=" + this._vm.pango
            document.getElementById("pango")!.setAttribute("href", pangoUrl)
            document.getElementById("pango")!.innerText = this._vm.pango.toString()

            /*
             * If any commentary notes are associated with the variant, show them in an HTML table.
             */
            if(this._vm.positionNotes.length > 0)
            {

                document.getElementById("direct-mutations-none")!.style.display = "none" // hide no comments label

                const directCommentsTable: HTMLTableElement = makeCommentsTable(this._vm.positionNotes)

                const directMutations = document.getElementById("direct-mutations")
                directMutations!.style.display = "block"
                directMutations!.appendChild(directCommentsTable)
            }
        }
    }

    // each element of notes becomes one row in the returned table
    function makeCommentsTable(notes: PositionNote[]): HTMLTableElement
    {
        const table: HTMLTableElement = document.createElement("table")
        table.className = "comments-table"
        const headerRow = document.createElement("tr")
        table.appendChild(headerRow)

        const positionHeader = document.createElement("th")
        positionHeader.innerText = "Position"
        headerRow.appendChild(positionHeader)

        const proteinIdHeader = document.createElement("th")
        proteinIdHeader.innerText = "Protein Id"
        headerRow.appendChild(proteinIdHeader)

        const mutationsHeader = document.createElement("th")
        mutationsHeader.innerText = "Mutations"
        headerRow.appendChild(mutationsHeader)

        const commentsHeader = document.createElement("th")
        commentsHeader.innerText = "Comments"
        headerRow.appendChild(commentsHeader)

        for(const note of notes)
        {
            let comments: string[] = note.comments
            if(comments.length == 0)
                comments = ["NA"];

            const dataRow = document.createElement("tr")
            table.appendChild(dataRow)

            const positionCell = document.createElement("td")
            positionCell.innerText = note.positionId.toString()
            dataRow.appendChild(positionCell)

            const proteinIdCell = document.createElement("td")
            proteinIdCell.innerText = note.proteinId.toString()
            dataRow.appendChild(proteinIdCell)

            const mutationsCell = document.createElement("td")
            mutationsCell.innerText = note.mutations.toString()
            dataRow.appendChild(mutationsCell)

            const commentsCell = document.createElement("td")
            dataRow.appendChild(commentsCell)
            for(const comment of comments)
            {
                const commentSpan = makeCommentElementFromText(comment.toString())
                commentsCell.appendChild(commentSpan)
            }
        }

        return table
    }

    function makeCommentElementFromText(text: string): HTMLDivElement
    {
        const divWrapper: HTMLDivElement = document.createElement("div");
        divWrapper.className = "comment"

        for(const part of text.split(/(\[[^\]]*])/)) // split along bibliographic notations
        {
            if(part.startsWith("[PMID")) // turn pub med reference into a hyperlink to the pubmed article
            {
                const pmid = part.replace("[", "").replace("]", "").replace("PMID", "")
                const a = document.createElement("a")
                a.href = "https://pubmed.ncbi.nlm.nih.gov/" + pmid + "/"
                a.target= "_blank"
                const linkText = document.createTextNode(part)
                a.appendChild(linkText)
                divWrapper.appendChild(a)

            }
            else if(part.startsWith("[http")) // turn hyperlink reference into an actual hyperlink
            {
                let url = part.substr(1)
                url = url.substr(0, url.length-1)
                const a = document.createElement("a")
                a.href = url
                a.target= "_blank"
                const linkText = document.createTextNode(part)
                a.appendChild(linkText)
                divWrapper.appendChild(a)
            }
            else // just general text
            {
                const text = document.createTextNode(part)
                divWrapper.appendChild(text)
            }
        }

        return divWrapper
    }
}