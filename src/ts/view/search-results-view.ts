namespace View
{
    import SearchResultsViewModel = Model.ViewModel.SearchResultsViewModel
    import RecordSummary = Model.ViewModel.VariantSummary
    import RegularString = Model.ViewModel.RegularString

    /**
     * A visualization of a `SearchResultsViewModel` instance.
     */
    export interface SearchResultsView
    {
        readonly userRequestedViewRecordEvent: TypedEvent<RecordSummary>

        readonly userRequestedResultsDownloadEvent: TypedEvent<void>

    }

    /**
     * Search results visualization based on http://tabulator.info/
     */
    export class SearchResultsViewTabulator implements SearchResultsView
    {
        private readonly _userRequestedViewRecordEventController = new TypedEventController<RecordSummary>()

        readonly userRequestedViewRecordEvent: TypedEvent<RecordSummary> = this._userRequestedViewRecordEventController

        private readonly _userRequestedResultsDownloadEventController = new TypedEventController<void>()

        readonly userRequestedResultsDownloadEvent: TypedEvent<void> = this._userRequestedResultsDownloadEventController

        private readonly _resultsDiv: HTMLElement | null

        private readonly _resultsHeader: HTMLElement | null

        private readonly _noResultsDiv: HTMLElement | null

        private readonly _spinner: HTMLElement | null

        private readonly _downloadButton: HTMLButtonElement | null

        // a comparator of regular strings compatible with the custom sorter contract defined at
        // http://tabulator.info/docs/4.0/sort
        private readonly _tabulatorRsSorter =
            (a: RegularString<string>, b: RegularString<string>) => a.toString().localeCompare(b.toString())

        // a comparator of dates compatible with the custom sorter contract defined at
        // http://tabulator.info/docs/4.0/sort
        private readonly _tabulatorDateSorter = (a: Date, b: Date) => a.getTime() - b.getTime()

        constructor(vm: SearchResultsViewModel, resultsDivId: string, resultsDivHeaderId: string,
                    noResultsDivId: string, spinnerId: string, downloadButtonId: string)
        {
            this._resultsDiv =  document.getElementById(resultsDivId)
            if(this._resultsDiv === null)
                throw new Error("results element not found")

            this._resultsHeader = document.getElementById(resultsDivHeaderId)
            if(this._resultsHeader === null)
                throw new Error("results-header element not found")

            this._noResultsDiv =  document.getElementById(noResultsDivId)
            if(this._noResultsDiv === null)
                throw new Error("no-results element not found")

            this._spinner = document.getElementById(spinnerId)
            if(this._spinner === null)
                throw new Error("spinner element not found")

            this._downloadButton = <HTMLButtonElement> document.getElementById(downloadButtonId)
            if(this._downloadButton === null)
                throw new Error("download button element not found")

            this._downloadButton.onclick = () => this._userRequestedResultsDownloadEventController.fire()

            // @ts-ignore TODO: get tabulator type def
            const tabulator = new Tabulator("#" + resultsDivId,
            {
                data: [],
                rowDblClick: (_: Event, row: any) => this._userRequestedViewRecordEventController.fire(row._row.data),
                layout: "fitColumns",
                responsiveLayout: "hide",
                tooltips: false,
                paginationSize: 50,
                movableColumns: true,
                columns: [
                    {title: "AccessionId", field: "accessionId",           width: 150, sorter:this._tabulatorRsSorter},
                    {title: "Repository",  field: "repository",            width: 150, sorter:this._tabulatorRsSorter},
                    {title: "Locality",    field: "sampleLocalityName",    width: 150, sorter:this._tabulatorRsSorter},
                    {title: "Region",      field: "sampleLocalityRegion",  width: 150, sorter:this._tabulatorRsSorter},
                    {title: "Country",     field: "sampleLocalityCountry", width: 150, sorter:this._tabulatorRsSorter},
                    {title: "Pango",       field: "pango",                 width: 150, sorter:this._tabulatorRsSorter},
                    {title: "CDC",         field: "cdcLabel",              width:  70, sorter:this._tabulatorRsSorter},
                    {title: "Collected",   field: "whenCollected",         width: 150, sorter:this._tabulatorDateSorter,
                                           formatter:"datetime",           formatterParams:
                                                                           {
                                                                              outputFormat:"MMM D, YYYY",
                                                                              invalidPlaceholder:"(invalid date)"
                                                                           },

                    }
                ]
            });

            let recordsInTabulatorCount = 0
            vm.addNewResultsAddedListener((newResults: readonly RecordSummary[]) =>
            {
                if(newResults.length === 0)  // don't want to set any styles to visible if given empty array
                    return

                tabulator.addData(newResults, false)
                recordsInTabulatorCount+=newResults.length

                // TODO: results-count to field
                document.getElementById("results-count")!.innerText = recordsInTabulatorCount.toString()
                this._resultsDiv!.style.visibility = "visible" // at least one row in tabulator so show table
                this._resultsHeader!.style.visibility = "visible" // at least one row in tabulator so show results-count
            })

            vm.addFreezeListener(() =>
            {
                if(recordsInTabulatorCount < 1)
                    this._noResultsDiv!.style.visibility = "visible" // 0 results for whole search, show no results msg

                this._spinner!.style.visibility = "hidden" // searching over, hide progress spinner
                this._downloadButton!.style.display = "block" // searching over, show download button
            })
        }
    }
}