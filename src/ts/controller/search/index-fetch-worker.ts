// needed by this file, this file's dependencies, or transitive dependencies
importScripts("/js/data-structures/strings.js")
importScripts("/js/data-structures/iterators.js")
importScripts("/js/data-structures/sets.js")
importScripts("/js/data-structures/set-lower-case-string.js")
importScripts("/js/model/data-origin.js")
importScripts("/js/model/pango.js")
importScripts("/js/model/genome-path.js")
importScripts("/js/model/url-fetching.js")
importScripts("/js/model/cdc-variant-of.js")
importScripts("/js/model/sample/sample-locality.js")
importScripts("/js/model/sample/sample-record.js")
importScripts("/js/model/search-request.js")
importScripts("/js/model/indices/index.js")
importScripts("/js/data-structures/numbers.js")
importScripts("/js/controller/search/index-fetch-worker-messages.js")

/**
 * A Web Worker that processes phrase search requests and responds with found genome matches. Expected to be called by
 * KeywordSearchController, but not a requirement.
 */
namespace Controller.Search
{
    import LowerCaseString = DataStructures.Strings.LowerCaseString
    import PosInt = DataStructures.Number.PosInt
    import IndexFetchWorkerRequestMessage = Controller.Search.IndexFetchWorkerRequestMessage
    import IndexFetchWorkerResponseMessagePartialMatches =
        Controller.Search.IndexFetchWorkerResponseMessagePartialMatches
    import SearchMode = Model.SearchRequest.SearchMode
    import LogicalSetReadOnly = DataStructures.Sets.LogicalSetReadOnly
    import Phrase = DataStructures.Strings.Phrase
    import ObjectIteratorAsync = DataStructures.Iterator.ObjectIteratorAsync
    import SearchRequestByPhraseSet = Model.SearchRequest.SearchRequestByPhraseSet
    import SearchRequestByDateRange = Model.SearchRequest.SearchRequestByDateRange
    import SampleRecord = Model.Sample.SampleRecord
    import Index = Model.Indices.Index
    import ServerSideIndex = Model.Indices.ServerSideIndex
    import SetLowerCaseString = DataStructures.Sets.SetLowerCaseString
    import LogicalSet = DataStructures.Sets.LogicalSet
    import CdcLabel = Model.CdcVariantOf.CdcLabel
    import ServerSideIndexLocalId = Model.Indices.ServerSideIndexLocalId
    import UrlFetcher = Model.UrlFetching.UrlFetcher;
    import UrlFetcherParallelizationLimited = Model.UrlFetching.UrlFetcherParallelizationLimited;

    /**
     * Define the application's server side indexes.
     */
    // TODO: constant 1000 should probably be moved to some configuration file
    const urlFetcher: UrlFetcher = new UrlFetcherParallelizationLimited(1000)
    const CdcLabelIndex:   Index = new ServerSideIndex(       "/data/indices/cdclabel/",   urlFetcher)
    const PangoIndex:      Index = new ServerSideIndex(       "/data/indices/pango/",      urlFetcher)
    const LocalityIndex:   Index = new ServerSideIndex(       "/data/indices/locality/",   urlFetcher)
    const RegionIndex:     Index = new ServerSideIndex(       "/data/indices/region/",     urlFetcher)
    const CountryIndex:    Index = new ServerSideIndex(       "/data/indices/country/",    urlFetcher)
    const LocalIdIndex:    Index = new ServerSideIndexLocalId("/data/indices/id/",         urlFetcher)
    const YearIndex:       Index = new ServerSideIndex(       "/data/indices/year/",       urlFetcher)
    const MonthIndex:      Index = new ServerSideIndex(       "/data/indices/month/",      urlFetcher)
    const DateIndex:       Index = new ServerSideIndex(       "/data/indices/date/",       urlFetcher)
    const RepositoryIndex: Index = new ServerSideIndex(       "/data/indices/repository/", urlFetcher)


    /**
     * When a message event is sent to this worker, unpack all the search phrases from the message and search
     * each index for each search phrase (exact or substring). As matches are found, respond to the caller by posting
     * back matched records progressively in batches of a size specified by the message event.
     *
     * All phrases with be converted to lower-case for searching and duplicate strings (after case adjustment)
     * will only be searched for once.
     *
     * @param messageEvent the search request
     */
    onmessage = function (messageEvent: MessageEvent)
    {
        const message: IndexFetchWorkerRequestMessage | null =
            IndexFetchWorkerRequestMessage.makeIndexFetchWorkerRequestMessageFromStructuredClone(messageEvent.data)

        if(message === null)
            throw Error("Invalid message: " + messageEvent.data.toString())

        const minTimeBetweenReportsMs = new PosInt(500) // rate limit calls to postMessage(...) to not flood UI thread
                                                        // with message processing tasks and make UI unresponsive.

        const responseMatchesChunkSize: PosInt = message.responseMatchesChunkSize;

        const reporter = new RateLimitedAggregateMatchesReporter(responseMatchesChunkSize, minTimeBetweenReportsMs)

        const enqueueAllMatchesToReporterTask: Promise<void> = message.searchRequest.which(
            (request:SearchRequestByPhraseSet) => searchByPhraseSet(request, reporter),
            (request:SearchRequestByDateRange) => searchByDateRange(request, reporter))

        enqueueAllMatchesToReporterTask.then(() => reporter.finishedEnqueueingAllMatches())
    }

    /**
     * Search for all records that match the given phrases and enqueue them for reporting via `matchesReporter`.
     *
     * @param request the phrases to search for and the search mode (i.e. AND/OR searching)
     * @param matchesReporter the destination for found matches
     * @return a promise that resolves when all searching and enqueuing to `matchesReporter` is complete
     */
    async function searchByPhraseSet(request: SearchRequestByPhraseSet,
                                     matchesReporter: RateLimitedAggregateMatchesReporter)
    {
        const phrases: LogicalSet<LowerCaseString> = new SetLowerCaseString()
        for(let phrase of  request.phrases)
            phrases.add(new LowerCaseString(phrase.asString.toLowerCase()))

        switch (request.mode)
        {
            case SearchMode.OR:
                await searchAndReportAnyWordMatches(phrases, matchesReporter)
                break;
            case SearchMode.AND:
                await searchAndReportAllWordMatches(phrases, matchesReporter)
                break;
            default:
                const unreachableAssignmentIfCodeCompiles: never = request.mode
                throw (unreachableAssignmentIfCodeCompiles) // wrap in parens to suppress IDE warning for inline var
        }
    }

    /**
     * Search for all records that match the given date range and enqueue them for reporting via `matchesReporter`.
     *
     * @param request the date range
     * @param matchesReporter the destination for found matches
     * @return a promise that resolves when all searching and enqueuing to `matchesReporter` is complete
     */
    async function searchByDateRange(request: SearchRequestByDateRange,
                                     matchesReporter: RateLimitedAggregateMatchesReporter)
    {

        // TODO: be smarter here about pulling down entire years or months instead of a day at a time if a search
        //       span is wide enough to cover a whole year or month.

        /*
         * Get all the unique integer days of month across the entire search timespan.
         */
        const uniqueDaysOfMonth: Set<number> = new Set<number>()
        // n.b. setDate(...) handles rollover of month and year (and day)
        for(let i = new Date(request.minDate); i.getTime() <= request.maxDate.getTime(); i.setDate(i.getDate() + 1))
            uniqueDaysOfMonth.add(i.getDate())

        /*
         * Search `DateIndex` looking for a matches by day of month, then filter out any records not in the search
         * timespan, and collect all the enqueuing tasks used to report found matches.
         */
        const isWithinSearchRange =
            (entry:SampleRecord) => entry.whenCollected.getTime() >= request.minDate.getTime() &&
                                    entry.whenCollected.getTime() <= request.maxDate.getTime()

        const enqueuingToMatchesReporterTasks: Promise<void>[] = []
        for(let dayOfMonth of uniqueDaysOfMonth)
        {
            const entriesInDateRange: ObjectIteratorAsync<SampleRecord> =
                DateIndex.fetchSamplesWithFeatureValue(dayOfMonth).where(isWithinSearchRange)

            const task = reportMatchesAsync(entriesInDateRange, matchesReporter, MatchCategory.Date)

            enqueuingToMatchesReporterTasks.push(task)
        }

        /*
         * Wait for all reporting tasks to finish sending matches.
         */
         await Promise.all(enqueuingToMatchesReporterTasks)
    }


    /**
     * Searches all the indices for any record that has any of the given phrases (exact or substring) and enqueues
     * all matches to `matchesReporter` passing at most `chunkSize` matches per invocation of
     * `matchesReporter.enqueueMatchesForReporting(...)`.
     *
     * @param phrases the phrases to search for
     * @param matchesReporter the destination for found matches
     * @return a promise that resolves when all searching and enqueuing to `matchesReporter` is complete
     */
    async function searchAndReportAnyWordMatches(phrases: LogicalSetReadOnly<LowerCaseString>,
                                                 matchesReporter: RateLimitedAggregateMatchesReporter)
    {
        const enqueuingToMatchesReporterTasks: Promise<void>[] = []
        for(let phrase of phrases)
        {
            const task =
                searchAndReportSingleWordMatchesAsync(phrase, matchesReporter)
            enqueuingToMatchesReporterTasks.push(task)
        }

        await Promise.all(enqueuingToMatchesReporterTasks)
    }

    /**
     * Searches the indices for all records that contain all the given phrases (exact or substring) and enqueues
     * all matches to `matchesReporter` passing at most `chunkSize` matches per invocation of
     * `matchesReporter.enqueueMatchesForReporting(...)`.
     *
     * @param phrases the phrases to search for
     * @param matchesReporter the destination for found matches
     * @return a promise that resolves when all searching and enqueuing to `matchesReporter` is complete
     */
    async function searchAndReportAllWordMatches(phrases: LogicalSetReadOnly<LowerCaseString>,
                                                 matchesReporter: RateLimitedAggregateMatchesReporter)
    {
        if(phrases.size < 1)
            return Promise.resolve()

        // tests if the given `entry` contains all `phrases` at any feature of `entry`
        function includesAllPhrasesFilter(entry: SampleRecord): boolean
        {
            const whenCollected = entry.whenCollected
            const whenCollectedMonthZeroBased = whenCollected.getMonth()
            const whenCollectedMonthOneBased = <1|2|3|4|5|6|7|8|9|10|11|12> (whenCollectedMonthZeroBased+1)

            for(let phrase of phrases)
            {
                const phraseAsString = phrase.toString()

                const toString = (p: Phrase | CdcLabel | null) => p === null ? "" : p.asString

                // n.b. the (...) around the pango check are really important because without them somehow the syntax
                // is legal, but the semantics are different, so phraseFound will take the value false when we want
                // it to be true.
                const phraseFound = toString(entry.sampleLocality.country).toLowerCase().includes(phraseAsString)  ||
                                    toString(entry.sampleLocality.locality).toLowerCase().includes(phraseAsString) ||
                                    toString(entry.sampleLocality.region).toLowerCase().includes(phraseAsString)   ||
                                    (entry.pango === null ?
                                        false : entry.pango.asString.toLowerCase().includes(phraseAsString))       ||
                                    toString(entry.cdcLabel).toLowerCase().includes(phraseAsString)                ||
                                    whenCollected.getFullYear().toString().toLowerCase().includes(phraseAsString)  ||
                                    whenCollected.getDate().toString().toLowerCase().includes(phraseAsString)      ||
                                    getMonthNameOf(whenCollectedMonthOneBased).includes(phraseAsString)            ||
                                    entry.accessionId.toLowerCase().includes(phraseAsString)                       ||
                                    entry.repository.toLowerCase().includes(phraseAsString)

                if(!phraseFound)
                    return false

            }

            return true
        }

        /*
         * Search each index for each phrase, but only report any found entry if it contains *all* phrases. The all
         * phrases constraint is enforced via the `includesAllPhrasesFilter`.
         */
        const enqueuingToMatchesReporterTasks: Promise<void>[] = []
        for(let phrase of phrases)
        {
            const task = searchAndReportSingleWordMatchesAsync(phrase, matchesReporter, includesAllPhrasesFilter)
            enqueuingToMatchesReporterTasks.push(task)
        }

        await Promise.all(enqueuingToMatchesReporterTasks)
    }

    function getMonthNameOf(number: 1|2|3|4|5|6|7|8|9|10|11|12): LowerCaseString
    {
        switch (number)
        {
            case 1:
                return new LowerCaseString("january")
            case 2:
                return new LowerCaseString("february")
            case 3:
                return new LowerCaseString("march")
            case 4:
                return new LowerCaseString("april")
            case 5:
                return new LowerCaseString("may")
            case 6:
                return new LowerCaseString("june")
            case 7:
                return new LowerCaseString("july")
            case 8:
                return new LowerCaseString("august")
            case 9:
                return new LowerCaseString("september")
            case 10:
                return new LowerCaseString("october")
            case 11:
                return new LowerCaseString("november")
            case 12:
                return new LowerCaseString("december")
            default:
                const exhaustiveSwitchCheck: never = (() => number)() // arrow func to suppress var redundant warning
                throw "If code compiles, this line is unreachable" + exhaustiveSwitchCheck
        }
    }

    /**
     * Searches the server side indices for all records that contain the given phrase (exact or substring) and enqueues
     * all matches to `matchesReporter` passing at most `chunkSize` matches per invocation of
     * `matchesReporter.enqueueMatchesForReporting(...)`.
     *
     * @param searchPhrase the phrase to search for
     * @param matchesReporter the destination for found matches
     * @param reportFilter an optional filter that can be used to reject records that contain the given phrase,
     *                     but that should be excluded from reporting. the filter returning `true` indicates a match
     *                     should be reported and `false` indicates the match should not be reported.
     * @return a promise that resolves when all searching and enqueuing to `matchesReporter` is complete
     */
    function searchAndReportSingleWordMatchesAsync(searchPhrase: LowerCaseString,
                                                   matchesReporter: RateLimitedAggregateMatchesReporter,
                                                   reportFilter: (_:SampleRecord) => boolean = (_ => true))
        : Promise<void>
    {
        // the indices to search within for `searchPhrase`
        const categoryAndIndex: readonly { category: MatchCategory, index: Index }[] =
            [
                { category: MatchCategory.Locality,   index: LocalityIndex },
                { category: MatchCategory.Region,     index: RegionIndex },
                { category: MatchCategory.Country,    index: CountryIndex },
                { category: MatchCategory.Pango,      index: PangoIndex },
                { category: MatchCategory.CDC,        index: CdcLabelIndex },
                { category: MatchCategory.Id,         index: LocalIdIndex },
                { category: MatchCategory.Year,       index: YearIndex },
                { category: MatchCategory.Month,      index: MonthIndex },
                { category: MatchCategory.Day,        index: DateIndex },
                { category: MatchCategory.Repository, index: RepositoryIndex }
            ]

        const enqueuingTasksAccum: Promise<void>[] = [] // all enqueuing tasks created by this method are placed here
        const enqueuingTasksAccumPopulatorTasks: Promise<void>[] = [] // the tasks of appending to enqueuingTasksAccum
        for(const pair of categoryAndIndex)
        {
            const populator: Promise<void> = // the task of appending to enqueuingTasksAccum
                reportGenomesFromIndexWithPhrase(searchPhrase, matchesReporter, reportFilter,
                                                 pair.category, pair.index, enqueuingTasksAccum)
            enqueuingTasksAccumPopulatorTasks.push(populator)
        }

        return new Promise<void>((resolve, reject) =>
        {
            // wait for enqueuingTasksAccum to be fully populated and then resolve only when all enqueuing tasks within
            // enqueuingTasksAccum are finished
            Promise.all(enqueuingTasksAccumPopulatorTasks)
                   .then(_ => Promise.all(enqueuingTasksAccum).then(_ => resolve()))
                   .catch(e => reject(e))
        })
    }

    /*
     * A function that performs the following steps:
     *
     * 1.) Lookup all the distinct phrases in the given index.
     *
     *     e.g. { "dallas", "houston", "london", "new york city", ... } if given the city index.
     *
     * 2.) If `searchPhrase` (e.g. "york") is present (as substring or exact) in the manifest, find those records in the
     *     index that both contain the phrase AND pass the given report filter and enqueue them to `matchesReporter`.
     *
     * 3.) Append a promise(s) that will resolve when all the enqueuing from step 2 is complete to `enqueuingTasksAccum`.
     *
     * 4.) Return a promise that resolves when the appending operation to `reportingTasksAccum` from 3.) is
     *     complete.
     */
    function reportGenomesFromIndexWithPhrase(searchPhrase: LowerCaseString,
                                              matchesReporter: RateLimitedAggregateMatchesReporter,
                                              reportFilter: (_: SampleRecord) => boolean = (_ => true), // TODO: since not last param, can delete default value?
                                              category: MatchCategory, index: Index,
                                              enqueuingTasksAccum: Promise<void>[]): Promise<void>
    {
        return index.fetchDistinctFeatureValuesAsync().then((knownPhrases: LogicalSetReadOnly<LowerCaseString>) =>
        {
            for(const knownPhrase of knownPhrases)
            {
                if (knownPhrase.includes(searchPhrase))
                {
                    const entriesWithPhraseAndFiltered: ObjectIteratorAsync<SampleRecord> =
                        index.fetchSamplesWithFeatureValue(knownPhrase).where(reportFilter)

                    const task: Promise<void> = reportMatchesAsync(entriesWithPhraseAndFiltered, matchesReporter,
                                                                   category)

                    enqueuingTasksAccum.push(task)
                }
            }
        })
        .catch(e => Promise.reject(e)) // TODO: is this needed?
    }

    /**
     * Splits all the elements of `unreportedMatches` into arrays of size `chunkSize` (except for one array that may be
     * smaller than `chunkSize` if the number of elements is not evenly divided by `chunkSize`), converts the arrays
     * into `Match` arrays, and passes each array to `matchesReporter.enqueueMatchesForReporting(...)` for reporting.
     *
     * @param unreportedMatches the matches to convert and pass to `matchesReporter.enqueueMatchesForReporting(...)`
     * @param matchesReporter the destination for matches.
     * @param category the feature of `GenomicIndexEntry` all `unreportedMatches` matched on
     * @return a promise that resolves when the last invocation of `matchesReporter.enqueueMatchesForReporting(...)`
     *         (if any) has returned.
     */
    function reportMatchesAsync(unreportedMatches: ObjectIteratorAsync<SampleRecord>,
                                matchesReporter: RateLimitedAggregateMatchesReporter, category: MatchCategory)
        : Promise<void>
    {
        const remainingUnreportedMatches = unreportedMatches // more descriptive name for method body, while keeping
                                                             // better descriptive name for function parameter
        return new Promise<void>((resolve, reject) =>
        {
            /*
             * Split the elements of remainingUnreportedMatches into chunks of at most chunkSize and stagger (in time)
             * their delivery to reportPartialMatches in minTimeBetweenReportsMs or later intervals.
             */
            async function resolveOrRecur()
            {
                const recordsChunk: SampleRecord[] =
                    await remainingUnreportedMatches.takeAsync(matchesReporter.maxMatchCountPerReport.asNumber)

                if (recordsChunk.length === 0) // if all matches have been read from remainingUnreportedMatches
                {
                    resolve()
                    // n.b. don't call matchesReporter.finishedEnqueueingAllMatches() here because matchesReporter
                    // is being used other places for reporting as well
                    return // end recursion
                }

                /*
                 * Convert chunk of sample records into corresponding reportable matches and report them.
                 */
                const matches: Match[] = []
                for (let record of recordsChunk)
                    matches.push(new Match(record, category))

                matchesReporter.enqueueMatchesForReporting(matches)

                await resolveOrRecur() // await so exceptions will bubble upwards
            }
            resolveOrRecur().catch(e => reject(e))
        })
    }

    /**
     * Buffers `Match` instances for future asynchronous reporting by a timer to this web worker's caller via
     * `postMessage`.
     *
     * Incrementally accepts `Match` instances into a private queue and then, at a constructor provided interval, a
     * timer automatically and repeatedly dequeues up to (constructor provided) `maxMatchCountPerReport` matches in the
     * background and posts them to this web worker's caller wrapped in a
     * `IndexFetchWorkerResponseMessagePartialMatches` message.
     *
     * Additional matches may be added to this reporter for reporting at any time provided
     * `finishedEnqueueingAllMatches()` has not been invoked.
     *
     * Once no additional matches are to be enqueued to this reporter, `finishedEnqueueingAllMatches()` should be
     * invoked. A side effect of this method is that after invocation, once all remaining non-reported matches have
     * been reported, a final `IndexFetchWorkerResponseMessageNoMoreMatches` will be posted to this web worker's caller.
     *
     * The expected use of a reporter is that one reporter instance will be created per `IndexFetchWorkerRequestMessage`
     * received. This reporter will then be passed to all functions participating in searching for phrase matches. When
     * matches are found by differing search contexts (e.g. scanning any of the indexes), the matches among all contexts
     * will be aggregated to the reporter for batched delivery to this web worker's caller.
     */
    class RateLimitedAggregateMatchesReporter
    {
        /**
         * A queue of matches that have been added to this reporter but not yet reported to this web worker's caller.
         * @private
         */
        private readonly matchesToReport: Match[] = []

        /**
         * The id of the interval timer used to dequeue and report matches or `null` if no interval timer is currently
         * active. An interval timer should only be active (and this field non-null) if `matchesToReport` is non-empty.
         * @private
         */
        private reporterIntervalTimerId: number | null = null

        /**
         * A (inverted) flag to indicate if `finishedEnqueueingAllMatches()` has ever been invoked.
         * @private
         */
        private acceptingNewMatches = true

        /**
         * Creates a new reporter that reports matches added via `enqueueMatchesForReporting(...)` to this web worker's
         * caller via a series of reports with each subsequent report being transmitted (at least) every
         * `minTimeBetweenReportsMs` milliseconds and containing at most `maxMatchCountPerReport` matches.
         *
         * @param maxMatchCountPerReport
         * @param minTimeBetweenReportsMs
         */
        constructor(readonly maxMatchCountPerReport: PosInt, private readonly minTimeBetweenReportsMs: PosInt)
        {
            this.assertConsistentState()
        }

        /**
         * Add matches to be reported in the future to this web worker's caller via `postMessage`.
         * @param matches the matches to report
         * @throws `Error` if matches is empty
         * @throws `Error` if `finishedEnqueueingAllMatches()` has previously been invoked.
         */
        enqueueMatchesForReporting(matches: Match[])
        {
            this.assertConsistentState()

            if(matches.length === 0)
                throw Error("matches must be non-empty")

            if (!this.acceptingNewMatches)
                throw Error("reporter has been closed for new matches")

            this.matchesToReport.push(...matches)

            // ensure a background timer is running to post matches to this worker's caller
            if (this.reporterIntervalTimerId === null)
                this.reporterIntervalTimerId = setInterval(() => { this.dequeueAndReportSomeNumberOfMatches() },
                                                           this.minTimeBetweenReportsMs.asNumber)

            this.assertConsistentState()
        }

        /**
         * Dequeues the next `maxMatchCountPerReport` (or all of the queue if smaller) matches from `matchesToReport`
         * and
         * @private
         */
        private dequeueAndReportSomeNumberOfMatches()
        {
            this.assertConsistentState()

            if(this.reporterIntervalTimerId === null) // method should only be called from timer, why is timer cleared?
                throw Error("state management bug: dequeueAndReportSomeNumberOfMatches invoked but id is null")

            if(this.matchesToReport.length === 0)
                throw Error("state management bug: matchesToReport is empty")

            /*
             * Dequeue at most `maxMatchCountPerReport` matches and send them to this worker's caller.
             */
            const matchesToReport: Match[] = this.matchesToReport.splice(0, this.maxMatchCountPerReport.asNumber)
            const matchesMessage = new IndexFetchWorkerResponseMessagePartialMatches(matchesToReport)
            RateLimitedAggregateMatchesReporter.postResponseMessage(matchesMessage)

            /*
             * If local match queue is empty, shut down the interval timer driving reporting. (Will be resumed if future
             * matches are enqueued.)
             *
             * If no more matches will every be enqueued (i.e. `finishedEnqueueingAllMatches()` has been invoked), then
             * send notice of no more matches to worker caller.
             */
            if(this.matchesToReport.length === 0)
            {
                clearInterval(this.reporterIntervalTimerId)
                this.reporterIntervalTimerId = null;

                if(!this.acceptingNewMatches)
                    RateLimitedAggregateMatchesReporter.postNoMoreMatchesMessage()
            }

            this.assertConsistentState()
        }

        finishedEnqueueingAllMatches()
        {
            this.assertConsistentState()

            if(!this.acceptingNewMatches)
                return

            this.acceptingNewMatches = false

            /*
             * If the interval timer is running, we rely on the interval to send the final no more matches message.
             * However, if this method is invoked at a time when the timer is shut down due to an empty queue it will
             * never be restarted; thus we must send the final message here.
             */
            if (this.reporterIntervalTimerId === null)
                RateLimitedAggregateMatchesReporter.postNoMoreMatchesMessage()

            this.assertConsistentState()
        }

        private assertConsistentState()
        {
            if(this.reporterIntervalTimerId === null)
            {
                if(this.matchesToReport.length > 0)
                    throw Error("no reporter interval, but matches remain to be reported")
            }
            else
            {
                if(this.matchesToReport.length < 1)
                    throw Error("reporter interval running, but no matches to report")
            }
        }

        private static postNoMoreMatchesMessage()
        {
            RateLimitedAggregateMatchesReporter.postResponseMessage(new IndexFetchWorkerResponseMessageNoMoreMatches())
        }

        private static postResponseMessage(message: IndexFetchWorkerResponseMessage)
        {
            // @ts-ignore - TypeScript compiler seems to think there is a mandatory 2nd parameter needed here
            // but there is not. see https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers
            // for example use of single argument version as the usual use case.
            postMessage(message)
        }

    }
}

