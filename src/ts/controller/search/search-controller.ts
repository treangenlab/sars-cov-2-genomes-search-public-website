namespace Controller.Search
{
    import PosInt = DataStructures.Number.PosInt
    import SearchRequest = Model.SearchRequest.SearchRequest
    import SearchMode = Model.SearchRequest.SearchMode
    import PhraseSet = Model.SearchRequest.PhraseSet
    import VariantSummary = Model.ViewModel.VariantSummary
    import TrimmedString = Model.ViewModel.TrimmedString
    import Phrase = DataStructures.Strings.Phrase
    import Pango = Model.Pango.Pango
    import SearchResultsViewModel = Model.ViewModel.SearchResultsViewModel
    import SearchResultsView = View.SearchResultsView
    import SearchResultsViewModelStringSet = Model.ViewModel.SearchResultsViewModelStringSet
    import RegularString = Model.ViewModel.RegularString
    import SearchRequestByPhraseSet = Model.SearchRequest.SearchRequestByPhraseSet
    import SearchRequestByDateRange = Model.SearchRequest.SearchRequestByDateRange
    import CdcLabel = Model.CdcVariantOf.CdcLabel
    import SampleRecord = Model.Sample.SampleRecord

    /**
     * The primary class for coordinating all program actions related to the viewing of search.html .
     */
    export class SearchController
    {
        /**
         * The search parameters to use when searching through genome records.
         */
        private readonly _searchRequest: SearchRequest

        /**
         * Whether `start()` has ever been called.
         */
        private _started = false

        /**
         * The search results to display to the user. Initially empty but populated during search.
         */
        private readonly _searchResultsViewModel: SearchResultsViewModel = new SearchResultsViewModelStringSet()

        /**
         * The results visualization displayed to the user.
         */
        private readonly _searchResultsView: SearchResultsView

        /**
         * The variant summaries that have been added to `_searchResultsViewModel`.
         */
        private readonly _searchResults: VariantSummary[] = []

        public constructor(makeSearchResultsView: (_: SearchResultsViewModel) => SearchResultsView)
        {
            this._searchResultsView = makeSearchResultsView(this._searchResultsViewModel)
            const anyQueryValue: string | null = new URLSearchParams(window.location.search).get("any")
            const allQueryValue: string | null = new URLSearchParams(window.location.search).get("all")
            const date1QueryValue: string | null = new URLSearchParams(window.location.search).get("date-1")
            const date2QueryValue: string | null = new URLSearchParams(window.location.search).get("date-2")

            if(date1QueryValue != null && date2QueryValue != null)
            {
                this._searchRequest = new SearchRequestByDateRange(new Date(date1QueryValue), new Date(date2QueryValue))
            }
            else
            {
               /*
                * If a "any" or "all" query parameter appears in the browser's URL, take the (URL decoded) value of that
                * parameter, split it along whitespace boundaries, and treat the words as search inputs.  The use of
                * "any" vs. "all" also dictates whether the words should be treated as AND vs OR during searching.
                *
                * For example, if the browser URL were:
                *
                *     http://127.0.0.1:8887/search.html?any=houston+dallas+austin
                *
                * then the search phrases would be:
                *
                *     { "houston", "dallas", "austin" }
                *
                * If no such query parameters are present, or both, or a single is present but describes an empty set,
                * redirect the browser to "/"
                */

                const queryValue: string | null = SearchController.selectNonNull(allQueryValue, anyQueryValue)
                if(queryValue === null)
                {
                    window.location.href = "/"
                    throw Error("Construction aborted due to page redirect.")
                }

                const queryWords: string[] = queryValue.split(/(\s+)/).map(e => e.trim()).filter(e => e.length > 0)
                if(queryWords.length < 1)
                {
                    window.location.href = "/"
                    throw Error("Construction aborted due to page redirect.")
                }

                this._searchRequest =
                    new SearchRequestByPhraseSet(anyQueryValue == null ? SearchMode.AND : SearchMode.OR,
                                                 new PhraseSet(queryWords))
            }
        }

        // if both null, return null. if both non-null, return null. otherwise return the single non-null value.
        private static selectNonNull(a: string | null, b: string | null): string | null
        {
            const bothNull = a === null && b === null
            const bothNonNull = a != null && b != null
            if (bothNull || bothNonNull)
                return null

            if (a != null)
                return a

            return b
        }

        // noinspection JSUnusedGlobalSymbols - start() called by search.html
        /**
         * Process a user submitted search directive (via "keywords" URL query parameter) and show the results. May
         * only be called once. Throws an error on every invocation but the first.
         */
        public start(): void
        {
            if (this._started)
                throw new Error("Controller is already started.")

            this._started = true

            const SrchCtr = SearchController
            this._searchResultsView.userRequestedViewRecordEvent.addListener(SrchCtr.handleUserRequestedDetailsOfRecord)
            this._searchResultsView.userRequestedResultsDownloadEvent.addListener(
                () => this.handleUserRequestedResultsDownload())

            const worker = new Worker("/js/controller/search/index-fetch-worker.js")
            let noMoreMatchesMessageSeen = false
            worker.onmessage = (messageEvent: MessageEvent) =>
            {
                const responseMessage: IndexFetchWorkerResponseMessage | null =
                    IndexFetchWorkerResponseMessage.makeFromStructuredClone(messageEvent.data);

                if(responseMessage === null)
                    throw Error("Invalid response message: " + messageEvent.data.toString())

                if(noMoreMatchesMessageSeen)
                    throw Error("Expected IndexFetchWorkerResponseMessageNoMoreMatches as final message")

                responseMessage.which(
                    (partialMatches: IndexFetchWorkerResponseMessagePartialMatches) =>
                    {
                        const partialResults: VariantSummary[] = partialMatches.matches.map(
                            (match:Match) => SearchController.translateEntryToSummary(match.entry))

                        this._searchResultsViewModel.add(partialResults)

                        for(const summary of partialResults)
                            this._searchResults.push(summary)
                    },
                    (_: IndexFetchWorkerResponseMessageNoMoreMatches) =>
                    {
                        this._searchResultsViewModel.freeze()
                        noMoreMatchesMessageSeen = true
                    })
            }

            worker.postMessage(new IndexFetchWorkerRequestMessage(this._searchRequest, new PosInt(500)))
        }

        private static translateEntryToSummary(entry: SampleRecord): VariantSummary
        {
            return {
                accessionId: new RegularString<"^\\S+$">(entry.accessionId, "^\\S+$"),
                pango: SearchController.convertToTrimmedString(entry.pango),
                repository: entry.repository,
                sampleLocalityCountry: new TrimmedString(entry.sampleLocality.country.asString),
                sampleLocalityName: SearchController.convertToTrimmedString(entry.sampleLocality.locality),
                sampleLocalityRegion: SearchController.convertToTrimmedString(entry.sampleLocality.region),
                whenCollected: entry.whenCollected,
                cdcLabel: SearchController.convertToTrimmedString(entry.cdcLabel)
            }

        }

        private static convertToTrimmedString(value: Phrase | Pango | CdcLabel | null): TrimmedString
        {
            if(value === null)
                return new TrimmedString("")

            if(value instanceof Phrase)
                return new TrimmedString(value.asString)

            return new TrimmedString(value.toString())
        }

        /**
         * Open a new tab (or window based on user's browser config) pointed at the URL for the details page of the
         * given genome.
         *
         * @param record the given genome.
         */
        private static handleUserRequestedDetailsOfRecord(record: VariantSummary)
        {
            switch (record.repository)
            {
                case "NCBI":
                    window.open("/variant.html?repository=NCBI&id=" + record.accessionId)
                    return
                default:
                    return function unreachable(): never
                    {
                        throw new Error("unreachable")
                    }()
            }
        }

        /**
         * Instruct the browser to download a tsv file version of the results table.
         */
        private handleUserRequestedResultsDownload() // called when the user clicks the download button
        {
            /*
             * Build a tab separated value string of the results table.
             */
            let downloadContents = "AccessionId\tRepository\tLocality\tRegion\tCountry\tPango\tCDC\tCollected";
            const removeTabs = (x: any) => x.toString().replace("\t", "    ")
            for(const result of this._searchResults)
            {
                downloadContents += "\n" + removeTabs(result.accessionId) + "\t" + removeTabs(result.repository) +
                                    "\t" + removeTabs(result.sampleLocalityName) + "\t" +
                                     removeTabs(result.sampleLocalityRegion) + "\t" +
                                     removeTabs(result.sampleLocalityCountry) + "\t" + removeTabs(result.pango) + "\t" +
                                     removeTabs(result.cdcLabel)  + "\t" +
                                     removeTabs(result.whenCollected.toLocaleDateString("en-US"))
            }

            /*
             * Instruct the browser to "download" the file, even though created in memory.
             */
            // @ts-ignore - defined in file-saver-2-0-4.js
            saveAs(new Blob([downloadContents], {type: "text/plain;charset=utf-8"}), "search-results.tsv");
        }
    }
}

