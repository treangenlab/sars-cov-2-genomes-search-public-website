/**
 * Defines the format of the request and response messages used by the Index Fetch Web Worker when processing requests
 * and responding to requesters.
 */


namespace Controller.Search
{
    import PosInt = DataStructures.Number.PosInt
    import SearchRequest = Model.SearchRequest.SearchRequest
    import SampleRecord = Model.Sample.SampleRecord

    /**
     * A request message type expected as input to the Index Fetch Web Worker to initiate a search of genome indices for
     * the message defined search keywords.
     */
    // n.b. class over interface because instances intended for posting from UI thread to web worker and as such will be
    // copied by the structured clone algorithm which strips out any non-properties from the clone.
    export class IndexFetchWorkerRequestMessage
    {
        /**
         * The search words of the request.
         */
        searchRequest: SearchRequest

        /**
         * As matches are found and progressively sent back to caller from the web worker, the maximum number of matches
         * that will be delivered per response message.
         */
        responseMatchesChunkSize: PosInt

        constructor(searchRequest: SearchRequest, responseMatchesChunkSize: PosInt)
        {
            this.searchRequest = searchRequest
            this.responseMatchesChunkSize = responseMatchesChunkSize
        }

        /**
         * @param clone an object that is a structured clone of an `IndexFetchWorkerRequestMessage` instance.
         * @return a corresponding IndexFetchWorkerRequestMessage` instance or `null` if the clone is not a
         *         'IndexFetchWorkerRequestMessage' structured clone
         */
        static makeIndexFetchWorkerRequestMessageFromStructuredClone(
            clone: { searchRequest: { mode: number, phrases: { elements: readonly { asString: string }[] } } |
                                    { minDate: Date, maxDate: Date },
                     responseMatchesChunkSize: {asNumber: number}})
            : IndexFetchWorkerRequestMessage | null
        {
            const responseMatchesChunkSize: PosInt | null =
                PosInt.makeFromStructuredClone(clone.responseMatchesChunkSize)

            if(responseMatchesChunkSize === null)
                return null

            const searchRequest: SearchRequest | null = SearchRequest.makeFromStructuredClone(clone.searchRequest)
            if(searchRequest === null)
                return null

            return new IndexFetchWorkerRequestMessage(searchRequest, responseMatchesChunkSize)
        }
    }

    /**
     * Mandatory base class for objects posted back to caller from Index Fetch Web Worker
     */
    // n.b. class over interface because instances intended for posting from UI thread to web worker and as such will be
    // copied by the structured clone algorithm which strips out any non-properties from the clone.
    // noinspection JSUnusedGlobalSymbols - used outside of file
    export abstract class IndexFetchWorkerResponseMessage
    {
        /**
         * A flag indicating the runtime type of the message.
         */
        // @ts-ignore - used in makeFromStructuredClone(...)
        private readonly _kind: IndexFetchWorkerResponseMessageKind;

        protected constructor(kind: IndexFetchWorkerResponseMessageKind)
        {
            this._kind = kind
        }

        // visitor pattern
        public abstract which(forPartialMatches: (_: IndexFetchWorkerResponseMessagePartialMatches) => void,
                              forNoMoreMatches:  (_: IndexFetchWorkerResponseMessageNoMoreMatches)  => void): void

        /**
         * @param clone an object that is a structured clone of an `IndexFetchWorkerResponseMessage` instance.
         * @return a corresponding IndexFetchWorkerRequestMessage` instance or `null` if the given clone was not
         *         a structured clone of an `IndexFetchWorkerResponseMessage`.
         */
        public static makeFromStructuredClone(clone: any): IndexFetchWorkerResponseMessage | null
        {
            if(clone._kind === undefined || typeof clone._kind !== 'number')
                return null

            if(clone._kind === IndexFetchWorkerResponseMessageKind.IndexFetchWorkerResponseMessagePartialMatches)
                return IndexFetchWorkerResponseMessagePartialMatches.makeFromStructuredClone(clone)

            if(clone._kind === IndexFetchWorkerResponseMessageKind.IndexFetchWorkerResponseMessageNoMoreMatches)
                return new IndexFetchWorkerResponseMessageNoMoreMatches()

            return null
        }
    }

    /**
     * Type flag on all `IndexFetchWorkerResponseMessage` instances to indicate which kind of message the instance is.
     */
    enum IndexFetchWorkerResponseMessageKind
    {
        IndexFetchWorkerResponseMessagePartialMatches, IndexFetchWorkerResponseMessageNoMoreMatches
    }

    /**
     * A `IndexFetchWorkerResponseMessage` message that indicates that some non-empty subset of matches are being
     * delivered to the caller.
     */
    // noinspection JSUnusedGlobalSymbols - used outside of file
    export class IndexFetchWorkerResponseMessagePartialMatches extends IndexFetchWorkerResponseMessage
    {
        constructor(readonly matches: readonly Match[])
        {
            super(IndexFetchWorkerResponseMessageKind.IndexFetchWorkerResponseMessagePartialMatches)

            if(matches.length < 1)
                throw Error("matches must be non-empty")
        }

        /**
         * @param clone an object that is a structured clone of an `IndexFetchWorkerResponseMessagePartialMatches`
         *              instance.
         * @return a corresponding IndexFetchWorkerResponseMessagePartialMatches` instance or `null` if the given clone
         *         was not a structured clone of an `IndexFetchWorkerResponseMessagePartialMatches`.
         */
        static makeFromStructuredClone(clone: { matches: { entry: { repository: string, accessionId: string,
                                                                    pango: { asString: string } | null,
                                                                    cdcLabel: { asString: string } | null,
                                                                    whenCollected: Date,
                                                                    sampleLocality: { region: {asString: string} | null,
                                                                                      locality: {asString: string}|null,
                                                                                      country: {asString: string}}}
                                                          category: number
                                                         }[]
                                               })
            : IndexFetchWorkerResponseMessagePartialMatches | null
        {

            const matches: Match[] = []
            for(let m of clone.matches)
            {
                const match: Match | null = Match.makeFromStructuredClone(m)
                if(match == null)
                    return null

                matches.push(match)
            }

            return new IndexFetchWorkerResponseMessagePartialMatches(matches)
        }

        // visitor pattern
        which(forPartialMatches: (_: IndexFetchWorkerResponseMessagePartialMatches) => void,
              _:  (_: IndexFetchWorkerResponseMessageNoMoreMatches) => void): void
        {
            forPartialMatches(this)
        }
    }

    /**
     * A `GenomicIndexEntry` that was matched during searching because of a hit on (or within) the entity's feature
     * specified by the given category.
     */
    export class Match
    {
        constructor(readonly entry: SampleRecord, readonly category: MatchCategory) {}

        /**
         * @param clone an object that is a structured clone of a `Match` instance.
         * @return a corresponding 'Match` instance or `null` if the given clone was not a structured clone of a
         *         `Match`.
         */
        static makeFromStructuredClone(clone: { entry: { repository: string, accessionId: string,
                                                         pango: { asString: string } | null,
                                                         cdcLabel: { asString: string } | null,
                                                         whenCollected: Date,
                                                         sampleLocality: { region: {asString: string} | null,
                                                                           locality: {asString: string} | null,
                                                                           country: {asString: string}}}
                                                         category: number
                                              })
            : Match | null
        {

            const entry: SampleRecord | null = SampleRecord.makeFromStructuredClone(clone.entry)
            if(entry == null)
                return null

            let category: MatchCategory | null = null
            for(let c of MatchCategories)
                if(clone.category === c)
                    category = c

            if(category === null)
                return null

            return new Match(entry, category)

        }
    }

    /**
     * The features of a `GenomicIndexEntry`.
     */
    export enum MatchCategory
    {
        Id, Repository,Locality, Region, Country, Pango, Year, Month,
        Day, Date, CDC
    }

    /**
     * The values of `MatchCategory`.
     */
    const MatchCategories = [MatchCategory.Id, MatchCategory.Repository, MatchCategory.Locality, MatchCategory.Region,
                             MatchCategory.Country, MatchCategory.Pango, MatchCategory.Year, MatchCategory.Month,
                             MatchCategory.Day, MatchCategory.Date, MatchCategory.CDC]

    /**
     * A `IndexFetchWorkerResponseMessage` message that indicates that all matches have been delivered to the caller.
     */
    // noinspection JSUnusedGlobalSymbols - used outside of file
    export class IndexFetchWorkerResponseMessageNoMoreMatches extends IndexFetchWorkerResponseMessage
    {
        constructor()
        {
            super(IndexFetchWorkerResponseMessageKind.IndexFetchWorkerResponseMessageNoMoreMatches);
        }

        // visitor pattern
        which(_: (_: IndexFetchWorkerResponseMessagePartialMatches) => void,
              forNoMoreMatches:  (_: IndexFetchWorkerResponseMessageNoMoreMatches) => void): void
        {
            forNoMoreMatches(this)
        }
    }
}