namespace Controller.VariantDetails
{
    import parseRepository = Model.DataOrigin.parseRepository
    import Repository = Model.DataOrigin.Repository
    import Variant = Model.ViewModel.Variant
    import ShowableView = View.ShowableView
    import RegularString = Model.ViewModel.RegularString
    import TrimmedString = Model.ViewModel.TrimmedString
    import VariantNotFoundView = View.VariantNotFoundView
    import VariantViewHtml = View.VariantViewHtml
    import makeNcbiGenomeDataPath = Model.Genomes.makeNcbiGenomeDataPath

    // noinspection JSUnusedGlobalSymbols - VariantDetailsController used by variant.html
    /**
     * The primary class for coordinating all program actions related to the viewing of variant.html.
     */
    export class VariantDetailsController
    {
        /**
         * The only `VariantDetailsController` instance.
         */
        public static readonly Singleton: VariantDetailsController = new VariantDetailsController()

        /**
         * Whether `start()` has ever been called.
         */
        private _started = false

        /**
         * The details about the variant to present to the view or `null` if the variant requested for display is
         * unknown.
         */
        private readonly _viewModel: Promise<Variant | null>

        /**
         * The renderer of variant details.
         */
        private readonly _view: Promise<ShowableView>

        private constructor() // locked down for singleton pattern
        {
            /*
             * Read from the URL query parameters the repository origin and id of the variant to display.
             */
            const repo: Repository | null =
                parseRepository(new URLSearchParams(window.location.search).get("repository") ?? "")
            const repoLocalId: string | null = new URLSearchParams(window.location.search).get("id")

            /*
             * Initialize the view and view model (asynchronously because of precursor network calls).
             */
            if (repo === null || repoLocalId === null) // if user did not specify which variant to show
            {
                this._view = Promise.resolve(new VariantNotFoundView())
                this._viewModel = Promise.resolve(null)
                return
            }

            switch (repo)
            {
                case "NCBI":
                    const variantDataPath: string = makeNcbiGenomeDataPath(repoLocalId)
                    const commentaryPath: string = variantDataPath.replace(".json", "-comments.json")
                    this._viewModel = this.makeViewModelAsync(variantDataPath, commentaryPath)
                    this._view = this._viewModel.then<ShowableView>((vm: Variant | null) =>
                    {
                        if(vm === null) // if could not locate data about variant
                            return new VariantNotFoundView()

                        const view = new VariantViewHtml(vm)

                        const downloadHandler = () => this.handleUserRequestedMutationsDownloadAsync()
                        view.userRequestedMutationsDownloadEvent.addListener(downloadHandler)

                        return view

                    })
                    break
                default:
                    const unreachable: never = (repo) // wrapped in () to avoid IDE warning of redundant variable
                    throw new Error("unreachable " + unreachable)
            }
        }

        // noinspection JSUnusedGlobalSymbols - start() called by search.html
        /**
         * Process a user submitted show details directive (via URL query parameters) and show the results. May only be
         * called once. Throws an error on every invocation but the first.
         */
        public async startAsync(): Promise<void>
        {
            if (this._started)
                throw Error("Controller is already started.")

            this._started = true;

            (await this._view).show()
        }

        // fetch the data from the given urls to create a view model
        //
        // e.g. http://host.com/data/genomes/ncbi/sra/err/416/504/ERR416504.json and
        //      http://host.com/data/genomes/ncbi/sra/err/416/504/ERR416504-comments.json
        private makeViewModelAsync(variantDataPath: string, variantCommentaryPath: string): Promise<Variant | null>
        {
            return new Promise<Variant | null >((resolve, _) =>
            {
                const fetchDataPathTask:       Promise<Response> = fetch(variantDataPath)
                const fetchCommentaryPathTask: Promise<Response> = fetch(variantCommentaryPath)

                Promise.all([fetchDataPathTask, fetchCommentaryPathTask]).then((responses: [Response, Response]) =>
                {
                    const profilePathResponse: Response = responses[0]
                    const commentaryPathResponse: Response = responses[1]

                    if (profilePathResponse.ok && commentaryPathResponse.ok)
                    {
                        const parseProfileResponseTask = profilePathResponse.json()
                        const parseCommentaryResponseTask = commentaryPathResponse.json()

                        Promise.all([parseProfileResponseTask, parseCommentaryResponseTask]).then((bodies: [any, any])=>
                            {
                                /**
                                 * Translate variant data json and variant commentary json to Variant instance
                                 */
                                const genomeDetails = bodies[0]
                                const commentary = bodies[1]

                                const id = new RegularString<"^\\S+$">(genomeDetails.o.i, "^\\S+$")
                                const repository: Repository | null = parseRepository(genomeDetails.o.r)

                                const locality = new TrimmedString(genomeDetails.l.l)
                                const region   = new TrimmedString(genomeDetails.l.r)
                                const country  = new TrimmedString(genomeDetails.l.c)

                                const pango = new TrimmedString(genomeDetails.p)

                                const vcfPath =
                                    new RegularString<"^\\S+$">(variantDataPath.replace("json", "vcf"), "^\\S+$")

                                const positionNotes: { positionId: RegularString<"^\\S+$">,
                                                       proteinId: RegularString<"^\\S+$">,
                                                       mutations: RegularString<"^\\S+$">,
                                                       comments: string[] }[] = []
                                for(const position in commentary)
                                {
                                    const proteinId: string = commentary[position].proteinId
                                    const mutations: string = commentary[position].mutations
                                    const note = { positionId: new RegularString<"^\\S+$">(position,"^\\S+$"),
                                                   proteinId: new RegularString<"^\\S+$">(proteinId,"^\\S+$"),
                                                   mutations: new RegularString<"^\\S+$">(mutations,"^\\S+$"),
                                                   comments: Array<string>() }

                                    for(const c of commentary[position].directMutations)
                                        note.comments.push(c)

                                    positionNotes.push(note)
                                }

                                const variant: Variant = { id: id, repository: repository!, vcfPath: vcfPath,
                                                           locality: locality, region: region, country: country,
                                                           pango: pango, positionNotes: positionNotes }

                                resolve(variant)

                            })
                    }
                    else
                    {
                        resolve(null) // can't find requested variant, so declare not found
                    }
                })
                .catch((_ => resolve(null))) // can't find requested variant, so declare not found
            });
        }

        /**
         * When the user clicks on the download mutations button, respond by packaging up the mutations as a file and
         * having the browser "download" the file.
         */
        private async handleUserRequestedMutationsDownloadAsync()
        {
            const vm = await this._viewModel

            if(vm === null) // null if view is variant not found, so nothing to download
                return

            /*
             * Build a tab separated value string of the mutation information.
             */
            let downloadContents = "Position\tProtein Id\tMutations\tComments";
            const removeTabs = (x: any) => x.toString().replace("\t", "    ")
            for(const note of vm.positionNotes)
            {
                downloadContents += "\n" + removeTabs(note.positionId) + "\t" + removeTabs(note.proteinId) + "\t" +
                                    removeTabs(note.mutations) + "\t"
                for(const c of note.comments)
                    downloadContents += removeTabs(c)
            }

            /*
             * Instruct the browser to "download" the file, even though created in memory.
             */
            // @ts-ignore - defined in file-saver-2-0-4.js
            saveAs(new Blob([downloadContents], {type: "text/plain;charset=utf-8"}), "mutations.tsv");
        }
    }
}