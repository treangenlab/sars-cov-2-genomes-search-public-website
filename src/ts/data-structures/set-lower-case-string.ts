namespace DataStructures.Sets
{
    import LowerCaseString = DataStructures.Strings.LowerCaseString

    /*
     * A `LogicalSet<LowerCaseString>` implementation backed by `Set<string>` for fast element testing.
     */
    export class SetLowerCaseString implements LogicalSet<LowerCaseString>
    {
        /**
         * The elements of the set in `String` form.
         */
        private _elementsAsString = new Set<string>();

        /**
         * The elements of the set in `LowerCaseString` form.
         */
        private _elementsAsLowerString: LowerCaseString[] = []

        // contract from super
        add(element: LowerCaseString)
        {
            if(this.has(element))
                return

            this._elementsAsLowerString.push(element)
            this._elementsAsString.add(element.toString())
        }

        // contract from super
        has(element: LowerCaseString): boolean
        {
            return this._elementsAsString.has(element.toString())
        }

        // contract from super
        get size(): number
        {
            return this._elementsAsString.size
        }

        // contract from super
        [Symbol.iterator](): Iterator<LowerCaseString>
        {
            return this._elementsAsLowerString[Symbol.iterator]()
        }
    }
}