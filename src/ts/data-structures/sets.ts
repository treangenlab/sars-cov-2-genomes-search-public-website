namespace DataStructures.Sets
{
    /**
     * A read only view of a set of objects.
     */
    export interface LogicalSetReadOnly<T> extends Iterable<T>
    {
        /**
         * Tests if the given element is present in the set using an implementation specific logical comparison of
         * elements.
         *
         * @param element the element to test for membership.
         */
        has(element: T): boolean

        /**
         * The number of elements in the set.
         */
        size: number
    }

    /**
     * A set of objects.
     */
    export interface LogicalSet<T> extends LogicalSetReadOnly<T>
    {
        /**
         * Adds the given element to the set if not already present as defined by an implementation specific logical
         * comparison. Otherwise does nothing.
         *
         * @param element the element to (potentially) add to the set
         */
        add(element: T): void
    }
}