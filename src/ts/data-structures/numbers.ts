namespace DataStructures.Number
{
    /**
     * An integer >= 1.
     */
    export class PosInt
    {
        /**
         * The nunber in `number` form.
         */
        readonly asNumber: number;

        public constructor(value: number)
        {
            this.asNumber = parseInt(value.toString())
            if(isNaN(this.asNumber) ||  this.asNumber !== value || value < 1)
                throw Error("given value is not an integer >=1. Given " + value)
        }

        /**
         * Creates a `PosInt` from the structured clone of a `PosInt`.
         *
         * @param clone a structured clone of a `PosInt` value.
         * @return a new `PosInt` instance corresponding to the clone or `null` if the clone was not a `PosInt`
         *         structured clone
         */
        public static makeFromStructuredClone(clone: {asNumber: number}): PosInt | null
        {
            try
            {
                return new PosInt(clone.asNumber)
            }
            catch (e)
            {
                return null
            }
        }
    }
}