namespace DataStructures.Strings
{
    /**
     * A string composed only of lowercase characters.
     */
    // @ts-ignore
    export class LowerCaseString
    {
        /**
         * Constructs the string from the given `string` value. If the given string contains any non-lowercase
         * characters an error will be thrown.
         *
         * @param _value the characters of the string.
         */
        public constructor(private _value: string)
        {
            for(let i = 0; i<_value.length; i++)
            {
                if(_value.charAt(i).toLowerCase() !== _value.charAt(i))
                    throw new Error("Given string must only be lower case characters. Found " + _value.charAt(i))
            }
        }

        /**
         * Tests if the string and the given string contain the same characters in the same order.
         *
         * @param other the given string.
         */
        public equals(other: LowerCaseString): boolean
        {
            return other._value === this._value
        }

        /**
         * Tests if the two strings contain the same characters in the same order.
         * @param a one string
         * @param b the other string
         */
        static equals(a: LowerCaseString, b: LowerCaseString): boolean
        {
            return a.equals(b)
        }

        /**
         * Returns true if searchString appears as a substring of the the string; otherwise, returns false.
         *
         * @param searchString search string
         */
        public includes(searchString: LowerCaseString | string): boolean
        {
            if(typeof searchString === 'string')
                return this._value.includes(searchString)

            return this._value.includes(searchString._value)
        }

        /**
         * @return the string in `string` form.
         */
        public toString(): string
        {
            return this._value
        }
    }

    /**
     * A non-empty trimmed string. E.g. "Houston" or "New York City".
     */
    // n.b. class over interface because intended to be copied by the structured clone algorithm which strips out any
    // non-properties from the clone.
    export class Phrase
    {
        /**
         * The phrase in string form.
         */
        readonly asString: string

        /**
         * @param phrase the phrase in string form. must be non-empty and contain no leading or training whitespace
         *        otherwise an error will be thrown.
         */
        constructor(phrase: string)
        {
            if(phrase.length === 0)
                throw Error("phrase may not be empty")

            if(phrase.trim() !== phrase)
                throw Error("phrase can not have leading or trailing whitespace")

            this.asString = phrase
        }
    }
}