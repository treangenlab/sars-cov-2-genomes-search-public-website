namespace DataStructures.Iterator
{
    /**
     * A sequence of zero or more objects that are traversed asynchronously.
     */
    export interface ObjectIteratorAsync<T extends Object>
    {
        /**
         * Returns (asynchronously) the next object in the sequence (via a promise) when that object is available or,
         * alternatively, returns `null` if the sequence is exhausted. If there is a problem accessing the next object
         * in the sequence, the returned promise will be set to the `rejected` state.
         *
         * Note: the return value of `null` unambiguously signals the end of the sequence because the value
         * `null` is not a member of the type `Object` and the type parameter `T` is constrained to `Object` at
         * its definition. Thus the value `null` may not be a member of the sequence (of objects).
         */
        getNextAsync(): Promise<T | null>

        /**
         * Returns (asynchronously) the next `count` number of objects in the sequence (via a promise) when all those
         * objects are available. If `count` is larger than the number of objects remaining in the sequence, returns
         * all the remaining objects in the sequence. If the sequence is exhausted, returns an empty array.
         *
         * If there is a problem accessing the next `count` number of objects in the sequence, or `count` is less than
         * zero, the returned promise will be set to the `rejected` state.
         *
         * @param count the maximum length of the promised array. must be >= 0 for the returned promise to resolve.
         */
        takeAsync(count: number): Promise<T[]>

        /**
         * Returns an iterator yielding only those elements of this iterator that pass the given filter.  The given
         * filter will be applied to each element of this iterator and elements will be included in the returned
         * iterator only if the filter returns `true` for a given element
         *
         * @param keep the filtering function that decides which elements are yielded by the returned iterator.
         * @return a filtered view of this iterator
         */
        where(keep: (element:T) => boolean):  ObjectIteratorAsync<T>
    }

    /**
     * Optional base class for implementors of ObjectIteratorAsync<T>
     */
    export abstract class ObjectIteratorAsyncBase<T extends Object> implements ObjectIteratorAsync<T>
    {
        // contract from interface
        public abstract getNextAsync(): Promise<T | null>

        // contract from interface
        public takeAsync(count: number): Promise<T[]>
        {
            if (count < 0)
                return Promise.reject("Given count was less than zero. Found: " + count.toString())

            if (count === 0)
                return Promise.resolve([])

            const outer = this
            return new Promise<T[]>((resolve, reject) =>
            {
                const accum: T[] = [] // the array to return once filled

                function resolveOrRejectOrRecur()
                {
                    outer.getNextAsync()
                    .then((next: T | null) =>
                    {
                        if (next === null) // if the sequence is exhausted
                        {
                            resolve(accum)
                            return
                        }

                        accum.push(next)

                        if (accum.length === count)
                        {
                            resolve(accum)
                            return
                        }

                        resolveOrRejectOrRecur()
                    })
                    .catch((error) => reject(error))
                }

                resolveOrRejectOrRecur()
            })
        }

        // contract from interface
        public where(keep: (element: T) => boolean): ObjectIteratorAsync<T>
        {
            return new FilteredIteratorDecorator(this, keep)
        }
    }

    class FilteredIteratorDecorator<T> extends ObjectIteratorAsyncBase<T>
    {
        constructor(private _source: ObjectIteratorAsync<T>, private _shouldKeep: (_:T) => boolean)
        {
            super()
        }

        // override from base
        public getNextAsync(): Promise<T | null>
        {
            return new Promise<T | null>((resolve, reject) =>
            {
                const outer = this
                function resolveOrRejectOrRecur()
                {
                    outer._source.getNextAsync()
                                 .then((next: T | null) =>
                                 {
                                     if(next === null || outer._shouldKeep(next))
                                     {
                                         resolve(next)
                                         return
                                     }

                                     resolveOrRejectOrRecur()
                                 })
                                 .catch(e => reject(e))
                }
                resolveOrRejectOrRecur()
            })
        }
    }
}