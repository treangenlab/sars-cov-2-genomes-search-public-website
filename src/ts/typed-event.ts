/**
 * A subscribable event that, when occurs, transmits a data message of type T to all listeners.
 *
 * @typeparam T the type of the data messages sent by the event.
 */
interface TypedEvent<T>
{
    /**
     * Subscribed the given listener function to the event. When the event occurs, listener will be invoked with
     * the data message.
     *
     * Subscribing the same listener n times to this event will cause n copies of the message to be transmitted to
     * the listener.
     *
     * @param listener the listener to subscribe to the event.
     */
    addListener(listener : (data:T) => void) : void
}

/**
 * A subscribable event that transmits a data message of type T to all listeners upon invocation of fire(T).
 *
 * @type <T> the type of the data messages sent by the event.
 */
class TypedEventController<T> implements TypedEvent<T>
{
    /**
     * The listeners of the event.
     */
    private readonly _listeners : Array<(data:T) => void> = [];

    // contract from interface
    addListener(listener : (data:T) => void)
    {
        this._listeners.push(listener);
    }

    /**
     * Broadcasts the given data message to all listeners synchronously.
     *
     * @param data the data message to transmit to all listeners.
     */
    fire(data : T)
    {
        for(let listener of this._listeners)
            listener(data);
    }
}
