package com.treangenlab.harvest_variant.sc2variants_com_aat;

import org.checkerframework.checker.units.qual.C;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Program
{
    public static void main(String[] args) throws InterruptedException
    {
        String serverAuthority = args[0];

        String chromeDriverPropertyKey = "webdriver.chrome.driver";
        if(!System.getProperties().contains(chromeDriverPropertyKey))
            System.setProperty(chromeDriverPropertyKey, "../../chromedriver-120-0-6099-109/chromedriver.exe");

        String firefoxDriverPropertyKey = "webdriver.gecko.driver";
        if(!System.getProperties().contains(firefoxDriverPropertyKey))
            System.setProperty(firefoxDriverPropertyKey, "../../geckodriver-0-31-0/geckodriver.exe");

        String edgeDriverPropertyKey = "webdriver.edge.driver";
        if(!System.getProperties().contains(edgeDriverPropertyKey))
            System.setProperty(edgeDriverPropertyKey, "../../edgedriver-120-0-2210-77/msedgedriver.exe");

        List<Supplier<WebDriver>> webDriverSuppliers;
        {
            String os = System.getProperty("os.name");
            if(os.startsWith("Windows"))
            {
                Supplier<WebDriver> chromeSupplier = () ->
                {
                    ChromeOptions chromeOptions = new ChromeOptions();
                    chromeOptions.addArguments("--remote-allow-origins=*");
                    return new ChromeDriver(chromeOptions);
                };

                webDriverSuppliers = Arrays.asList(chromeSupplier, FirefoxDriver::new, EdgeDriver::new);
            }
            else
            {
                throw new RuntimeException("Unknown OS");
            }
        }

        for(Supplier<WebDriver> driverSupplier : webDriverSuppliers)
        {
            WebDriver driver = driverSupplier.get();
            runTestsForDriver(driver, serverAuthority);
            driver.quit();
        }

        System.out.println("=================");
        System.out.println("All tests passed.");
        System.out.println("=================");
    }

    private static void runTestsForDriver(WebDriver driver, String serverAuthority) throws InterruptedException
    {
        // it is important to maximize the screen before checking for values in the search results table because if the
        // window is too narrow Tabulator will remove table columns from the DOM and thus expected values of the table
        // will not be present in the DOM. This impacts checks like assertCdcLabelAlways(...)
        driver.manage().window().maximize();

        userStory1(driver, serverAuthority);
        userStory2(driver, serverAuthority);
        userStory3(driver, serverAuthority);
        userStory4(driver, serverAuthority);
        issue34(driver, serverAuthority);
    }

    private static void userStory4(WebDriver driver, String serverAuthority)
    {
        gotoIndexPage(driver, serverAuthority);

        typeTextToElementWithId("search-input", "nil", driver).submit();
        waitUntilUrlContains("search.html", driver);

        waitUntilElementWithTagNameIsVisible("h1", driver);
        assertTrue(getElementByTagName("h1", driver).getText().equals("No Matches Found"));
        getElementWithText("button", "Search Again", driver).click();
    }

    private static void userStory1(WebDriver driver, String serverAuthority) throws InterruptedException
    {
        gotoIndexPage(driver, serverAuthority);

        typeTextToElementWithId("search-input", "alpha", driver).submit();
        inspectSearchResultsForKeywordAlpha(driver);
    }

    private static void userStory2(WebDriver driver, String serverAuthority) throws InterruptedException
    {
        gotoIndexPage(driver, serverAuthority);

        followLinkWithText("help", driver);
        waitUntilUrlContains("help.html", driver);
        assertTrue("About".equals(driver.getTitle()));

        followLinkWithText("advanced", driver);
        waitUntilUrlContains("advanced.html", driver);
        assertTrue("Advanced Search".equals(driver.getTitle()));
        typeTextToElementWithId("any-input", "alpha", driver).submit();
        inspectSearchResultsForKeywordAlpha(driver);
    }

    private static void userStory3(WebDriver driver, String serverAuthority) throws InterruptedException
    {
        driver.get("http://" + serverAuthority + "/search.html?any=alpha");
        waitUntilUrlContains(serverAuthority, driver);
        inspectSearchResultsForKeywordAlpha(driver);
    }

    private static void issue34(WebDriver driver, String serverAuthority)
    {
        driver.get("http://" + serverAuthority + "/search.html?any=err5");
        waitUntilUrlContains(serverAuthority, driver);
        waitUntilUrlContains("search.html", driver);
        waitUntilElementWithIdIsInvisible("spinner", driver);
        assertOneOf(getElementById("results-count", driver).getText(), "400", "5341"); // two results for test and prod
    }

    private static void gotoIndexPage(WebDriver driver, String serverAuthority)
    {
        driver.get("http://" + serverAuthority);
        waitUntilUrlContains(serverAuthority, driver);

        assertTrue(getElementByTagName("h1", driver).getText().equals("Search Harvest SARS-CoV-2 Variant Info"));
        assertTrue("SARS-CoV-2 Variant Genomes Search".equals(driver.getTitle()));
    }

    private static void inspectSearchResultsForKeywordAlpha(WebDriver driver) throws InterruptedException
    {
        waitUntilUrlContains("search.html", driver);
        waitUntilElementWithIdIsInvisible("spinner", driver);
        assertOneOf(getElementById("results-count", driver).getText(), "110", "6695"); // two results for test and prod
        assertCdcLabelAlways("Alpha", driver);

        getElementWithText("div", "AccessionId", driver).click(); // sort so rows below appear in test and prod datasets
        assertResultRowPresent("ERR4439312", "NCBI", "", "England", "United Kingdom", "B.1.1.70", "Alpha", "Jul 3, 2020",
                                driver);
        assertResultRowPresent("ERR4584996", "NCBI", "", "Northern Ireland", "United Kingdom", "B.1.1.74", "Alpha",
                               "Aug 1, 2020", driver);

        String windowHandleBeforeOpeningTab = driver.getWindowHandle();
        doubleClickOnResultRowWithAccessionId("ERR4439312", driver);

        Thread.sleep(Duration.ofSeconds(10).toMillis());

        String tabHandle;
        {
            Set<String> windowHandles = driver.getWindowHandles();
            if (windowHandles.size() != 2)
                throw new RuntimeException("expected 2 tabs to be open");

            windowHandles.remove(windowHandleBeforeOpeningTab);
            tabHandle = windowHandles.iterator().next();
        }

        driver.switchTo().window(tabHandle);
        assertAVariantGenomePage(driver);
        assertEquals("undefined", getElementById("locality", driver).getText());
        assertEquals("England", getElementById("region", driver).getText());
        assertEquals("United Kingdom", getElementById("country", driver).getText());
        assertEquals("B.1.1.70", getElementById("pango", driver).getText());
        assertNumberOfMutations(35, driver);
        assertNoCommentaryMutation(1274, "Nsp2", "Val157Phe", driver);
        assertNoCommentaryMutation(3045, "Nsp3", "Pro109Leu", driver);
        assertCommentaryMutation(14408, "Nsp12", "Pro323Leu", "The P323L mutation in the Nsp12 polymerase is", driver);
        assertIsLinkWithText("id", "ERR4439312", "https://www.ncbi.nlm.nih.gov/sra/?term=ERR4439312", driver);
        assertIsLinkWithText("vcf", "ERR4439312.vcf", "/data/genomes/ncbi/sra/ERR/443/931/ERR4439312.vcf", driver);

        driver.close();
        driver.switchTo().window(windowHandleBeforeOpeningTab);
    }

    private static void assertIsLinkWithText(String id, String text, String urlEnding, WebDriver driver)
    {
        WebElement element = getElementById(id, driver);
        assertTrue(element.getText().equals(text));
        assertTrue(element.getAttribute("href").endsWith(urlEnding));
    }

    private static void assertAVariantGenomePage(WebDriver driver)
    {
        waitUntilUrlContains("variant.html", driver);
        getElementById("download-mutations", driver);
        assertTrue("SARS-CoV-2 Variant Genome".equals(driver.getTitle()));
    }

    private static void assertNoCommentaryMutation(int position, String proteinId, String mutations, WebDriver driver)
    {
        List<WebElement> tableRows = driver.findElements(By.tagName("tr"));
        tableRows.remove(0); // skip header row

        int rowMatchCount = 0;
        for(WebElement tableRow : tableRows)
        {
            if(!tableRow.findElements(By.tagName("th")).isEmpty())
                continue; // skip header row

            List<WebElement> cells = tableRow.findElements(By.tagName("td"));

            if(cells.get(0).getText().equals(position + ""))
            {
                assertTrue(cells.get(1).getText().trim().equals(proteinId.trim()));
                assertTrue(cells.get(2).getText().trim().equals(mutations.trim()));
                assertTrue(cells.get(3).getText().trim().equals("NA"));
                rowMatchCount++;
            }
        }

        if(rowMatchCount != 1)
            throw new RuntimeException("Expected row match count of 1");
    }

    private static void assertCommentaryMutation(int position, String proteinId, String mutations, String commentPhrase,
                                                 WebDriver driver)
    {
        List<WebElement> tableRows = driver.findElements(By.tagName("tr"));
        tableRows.remove(0); // skip header row

        int rowMatchCount = 0;
        for(WebElement tableRow : tableRows)
        {
            List<WebElement> cells = tableRow.findElements(By.tagName("td"));

            if(cells.get(0).getText().equals(position + ""))
            {
                assertTrue(cells.get(1).getText().trim().equals(proteinId.trim()));
                assertTrue(cells.get(2).getText().trim().equals(mutations.trim()));
                assertTrue(cells.get(3).getText().trim().contains(commentPhrase.trim()));
                rowMatchCount++;
            }
        }

        if(rowMatchCount != 1)
            throw new RuntimeException("Expected row match count of 1");
    }

    private static void assertNumberOfMutations(int mutationRowCount, WebDriver driver)
    {
        assertTrue(mutationRowCount == driver.findElements(By.tagName("tr")).size());
    }

    private static void doubleClickOnResultRowWithAccessionId(String accessionId, WebDriver driver)
    {
        for(WebElement resultRow : driver.findElements(By.className("tabulator-row")))
        {
            List<WebElement> cells = resultRow.findElements(By.className("tabulator-cell"));
            WebElement firstCell = cells.get(0);

            if (firstCell.getText().equals(accessionId))
            {
                new Actions(driver).doubleClick(firstCell).perform();
                return;
            }
        }

        throw new RuntimeException("no match for id " + accessionId);
    }

    private static void assertResultRowPresent(String accessionId, String repository, String locality, String region,
                                               String country, String pango, String cdc, String collected,
                                               WebDriver driver)
    {
        int rowMatchCount = 0;
        for(WebElement resultRow : driver.findElements(By.className("tabulator-row")))
        {
            List<WebElement> cells = resultRow.findElements(By.className("tabulator-cell"));

            if(cells.get(0).getText().equals(accessionId))
            {
                assertTrue(cells.get(1).getText().trim().equals(repository.trim()));
                assertTrue(cells.get(2).getText().trim().equals(locality.trim()));
                assertTrue(cells.get(3).getText().trim().equals(region.trim()));
                assertTrue(cells.get(4).getText().trim().equals(country.trim()));
                assertTrue(cells.get(5).getText().trim().equals(pango.trim()));
                assertTrue(cells.get(6).getText().trim().equals(cdc.trim()));
                assertTrue(cells.get(7).getText().trim().equals(collected.trim()));

                rowMatchCount++;
            }
        }

        if(rowMatchCount != 1)
            throw new RuntimeException("Expected row match count of 1");
    }

    private static void assertCdcLabelAlways(String value, WebDriver driver)
    {
        List<WebElement> tabCells = driver.findElements(By.className("tabulator-cell"));
        List<WebElement> cdcCells =
                tabCells.stream()
                        .filter((WebElement e) -> e.getAttribute("role").equals("gridcell"))
                        .filter((WebElement e) -> e.getAttribute("tabulator-field").equals("cdcLabel"))
                        .collect(Collectors.toUnmodifiableList());

        if(cdcCells.isEmpty())
            throw new RuntimeException("no matches");

        for(WebElement cell : cdcCells)
            assertTrue(cell.getText().equals(value));
    }

    private static void waitUntilElementWithIdIsInvisible(String elementId, WebDriver driver)
    {
        makeWait(driver).until(ExpectedConditions.invisibilityOf(getElementById(elementId, driver)));
    }

    private static void waitUntilElementWithTagNameIsVisible(String tagName, WebDriver driver)
    {
        makeWait(driver).until(ExpectedConditions.visibilityOf(getElementByTagName(tagName, driver)));
    }

    private static WebElement typeTextToElementWithId(String elementId, String text, WebDriver driver)
    {
        WebElement element = focusElementWithId(elementId, driver);
        element.sendKeys(text);
        return element;
    }

    private static WebElement focusElementWithId(String elementId, WebDriver driver)
    {
        WebElement element = getElementById(elementId, driver);
        element.click();
        return element;
    }

    private static void followLinkWithText(String text, WebDriver driver)
    {
        getLinkWithText(text, driver).click();
    }

    private static WebElement getElementWithText(String tagName, String text, WebDriver driver)
    {
        for(WebElement element : driver.findElements(By.tagName(tagName)))
            if(element.getText().equals(text))
                return element;

        throw new RuntimeException("Not found");

    }

    private static WebElement getLinkWithText(String text, WebDriver driver)
    {
        return waitUntilElementLocated(By.linkText(text), driver);
    }

    private static WebElement getElementByTagName(String tagName, WebDriver driver)
    {
        return waitUntilElementLocated(By.tagName(tagName), driver);
    }

    private static WebElement getElementById(String elementId, WebDriver driver)
    {
        return waitUntilElementLocated(By.id(elementId), driver);
    }

    private static WebElement waitUntilElementLocated(By selector, WebDriver driver)
    {
        return makeWait(driver).until(ExpectedConditions.presenceOfElementLocated(selector));
    }

    private static void waitUntilUrlContains(String urlPart, WebDriver driver)
    {
        Wait<WebDriver> wait = makeWait(driver);
        wait.until(ExpectedConditions.urlContains(urlPart));
    }

    private static Wait<WebDriver> makeWait(WebDriver driver)
    {
        return new FluentWait<>(driver).withTimeout(Duration.ofMinutes(10))
                                       .pollingEvery(Duration.ofSeconds(5))
                                       .ignoring(NoSuchElementException.class)
                                       .ignoring(TimeoutException.class) // rendering issue-34 test takes a while
                                       .ignoring(ScriptTimeoutException.class); // rendering issue-34 test takes a while

    }

    private static void assertOneOf(Object value, Object... objects)
    {
        for(Object o : objects)
            if(value.equals(o))
                return;

        throw new RuntimeException("assertion failed");
    }

    private static <T> void assertEquals(T expected, T found)
    {
        if(expected.equals(found))
            return;

        throw new RuntimeException("Expected " + expected +" but found " + found);
    }

    private static void assertTrue(boolean predicate)
    {
        if(predicate)
            return;

        throw new RuntimeException("assertion failed");
    }
}
