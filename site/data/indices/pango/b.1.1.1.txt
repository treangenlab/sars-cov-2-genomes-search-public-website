{"p":"B.1.1.1","w":{"d":8,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4599808"}}
{"p":"B.1.1.1","w":{"d":13,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4598024"}}
{"p":"B.1.1.1","w":{"d":6,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4597254"}}
{"p":"B.1.1.1","w":{"d":2,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4585050"}}
{"p":"B.1.1.1","w":{"d":10,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4597686"}}
{"p":"B.1.1.1","w":{"d":20,"y":2020,"m":8},"l":{"r":"Northern Ireland","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4599241"}}
{"p":"B.1.1.1","w":{"d":28,"y":2020,"m":3},"l":{"c":"Austria"},"o":{"r":"NCBI","i":"ERR4704313"}}
{"p":"B.1.1.1","w":{"d":7,"y":2020,"m":6},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4640548"}}
{"p":"B.1.1.1","w":{"d":6,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4597105"}}
{"p":"B.1.1.1","w":{"d":12,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4598423"}}
{"p":"B.1.1.1","w":{"d":30,"y":2020,"m":7},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4584974"}}
{"p":"B.1.1.1","w":{"d":3,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4585087"}}
{"p":"B.1.1.1","w":{"d":2,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4599722"}}
{"p":"B.1.1.1","w":{"d":3,"y":2020,"m":4},"l":{"c":"Austria"},"o":{"r":"NCBI","i":"ERR4702573"}}
{"p":"B.1.1.1","w":{"d":11,"y":2020,"m":3},"l":{"c":"Austria"},"o":{"r":"NCBI","i":"ERR4702577"}}
{"p":"B.1.1.1","w":{"d":13,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4598413"}}
{"p":"B.1.1.1","w":{"d":1,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4599813"}}
{"p":"B.1.1.1","w":{"d":5,"y":2020,"m":5},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4248116"}}
{"p":"B.1.1.1","w":{"d":25,"y":2020,"m":10},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4834063"}}
{"p":"B.1.1.1","w":{"d":27,"y":2020,"m":7},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4584902"}}
{"p":"B.1.1.1","w":{"d":12,"y":2020,"m":5},"l":{"c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4249855"}}
{"p":"B.1.1.1","w":{"d":27,"y":2020,"m":3},"l":{"c":"Austria"},"o":{"r":"NCBI","i":"ERR4704312"}}
{"p":"B.1.1.1","w":{"d":18,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4598950"}}
{"p":"B.1.1.1","w":{"d":6,"y":2020,"m":3},"l":{"c":"Austria"},"o":{"r":"NCBI","i":"ERR4704542"}}
{"p":"B.1.1.1","w":{"d":21,"y":2020,"m":8},"l":{"r":"Scotland","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4599256"}}
{"p":"B.1.1.1","w":{"d":8,"y":2020,"m":3},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4205823"}}
{"p":"B.1.1.1","w":{"d":31,"y":2020,"m":7},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4599923"}}
{"p":"B.1.1.1","w":{"d":13,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4598036"}}
{"p":"B.1.1.1","w":{"d":21,"y":2020,"m":8},"l":{"r":"Scotland","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4599490"}}
{"p":"B.1.1.1","w":{"d":1,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4599707"}}
{"p":"B.1.1.1","w":{"d":2,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4585097"}}
{"p":"B.1.1.1","w":{"d":3,"y":2020,"m":7},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4633961"}}
{"p":"B.1.1.1","w":{"d":2,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4585074"}}
{"p":"B.1.1.1","w":{"d":9,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4644305"}}
{"p":"B.1.1.1","w":{"d":27,"y":2020,"m":7},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4584830"}}
{"p":"B.1.1.1","w":{"d":5,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4585049"}}
{"p":"B.1.1.1","w":{"d":31,"y":2020,"m":7},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4584973"}}
{"p":"B.1.1.1","w":{"d":10,"y":2020,"m":5},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4249018"}}
{"p":"B.1.1.1","w":{"d":8,"y":2020,"m":3},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4207573"}}
{"p":"B.1.1.1","w":{"d":2,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4585251"}}
{"p":"B.1.1.1","w":{"d":2,"y":2020,"m":8},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4585139"}}
{"p":"B.1.1.1","w":{"d":3,"y":2020,"m":3},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4206210"}}
{"p":"B.1.1.1","w":{"d":3,"y":2020,"m":4},"l":{"c":"Austria"},"o":{"r":"NCBI","i":"ERR4704320"}}
{"p":"B.1.1.1","w":{"d":12,"y":2020,"m":6},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR4632394"}}
{"p":"B.1.1.1","w":{"d":6,"y":2020,"m":12},"l":{"r":"England","c":"United Kingdom"},"o":{"r":"NCBI","i":"ERR5042265"}}
