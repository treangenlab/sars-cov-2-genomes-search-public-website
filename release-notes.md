# Release Notes

## v1.4.0
* Add link to gitlab page on landing page.
* Add example query button to landing page.
* Add CC BY license statement to landing page.
* Add output screenshot to help page.

## v1.3.2
* Fix issue with searching by SRA id not returning any results.

## v1.3.1
* Fixed issue with search match reporting to UI thread  being limited on per index basis instead of per search basis.

## v1.3.0

* Fixed issue with many parallel index network requests being issued in Chrome causing Chrome to fail requests.
* Use `/data/genomes/` as the source for result information when a search matches on an `id` feature instead
  of `/data/indicies/id/[id].txt` (which is now removed).

## v1.2.0

* New Pango lineages `BF.1.1` and `BA.5.6` supported.
* Updated record count in about page to reflect most recent data.