# Public Variant Genome Search Site

## Description

This codebase defines a browser based tool for visualizing and searching a database of SARS-CoV-2 variant genomes. It
is intended to be deployed as a static website with the purpose of providing a user interface to search a database of 
genomes defined in a specific file/directory format (described below) residing in a server side directory. 

## Static File Database Format

The tool expects the database files to be located at the (web) path `/data`. The data directory should be organized into
two main sub-folders, `/data/genomes/ncbi/sra/` (the genomes folder) and `/data/indices` (the indices folder).

### Genomes Folder Format

The genomes folder holds the variant call format files (VCF) that define the variant genomes. Each vcf file is named
after the NCBI SRA Run Id that gave rise to its form. For example, VCF file `DRR286884.vcf` corresponds to Run Id
`DRR286884`. VCF file placement is determined by the name of the VCF file. For each group of three characters 
(excluding the final group) in the file's name, a directory is created. For example `DRR286884.vcf` is placed in the 
folder `/data/genomes/ncbi/sra/DRR/286`. Accompanying each VCF file in the directory are two additional files files 
named (without loss of generality) `DRR286884.json` and `DRR286884-comments.json`.

The first file contains  metadata about the SRA run associated with the VCF in JSON, for example:

```
{"p":"B.1.1.7","c":"Alpha","w":{"d":27,"y":2021,"m":1},"l":{"c":"Ireland","l":"Dublin"},"o":{"r":"NCBI","i":"ERR5284959"}}
```

The `p` entry encodes the Pangolin label of the sample. The `c` entry encodes the CDC Label corresponding to the 
Pangolin label (if any). The `w` entry encodes the date on which the sample was collected. The `l` entry encodes where
the sample was collected with `c` representing Country, `r` Region, and `l` Locality. The `o` entry encodes the SRA
Run Id of the sample in the `i` field. The `r` field in this version of the tool is always `NCBI`.

The second file contains biocuration information about the variations defined in the VCF file. Each field in this JSON
file is keyed by the nucleotide position of the described variation.

### Indices Folder Format

The indices folder contains 10 indexes of various metadata features of each run/variant. Each folder is named after
the index it encodes. The folders are:

```
/data/indices/cdclabel
/data/indices/country
/data/indices/data
/data/indices/id
/data/indices/locality
/data/indices/month
/data/indices/pango
/data/indices/region
/data/indices/repository
/data/indices/year
```

Inside each folder is a set of text files named after each index value. For example, the file 
`/data/indices/country/japan.txt` contains JSON entries for those samples of the database collected in Japan.  The
format of the file is one JSON entry per line with the format of the JSON entry being the same as described above. In
addition to these named files, each folder contains the file `_.txt` which lists the names of the files in the directory
without their `.txt` extension (excluding `_.txt`).

## Building

This project contains a mixture of `html`, `css`, `js`, and `TypeScript` files. The site can be built with the 
[WebStorm](https://www.jetbrains.com/webstorm/)  tool using the project files located at `/tool-files/webstorm-2019`.

## Support

This work is supported by the Centers for Disease Control and Prevention (CDC) contract 75D30121C11180.
## License

MIT License

Copyright (c) 2022 Treangen Lab.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.